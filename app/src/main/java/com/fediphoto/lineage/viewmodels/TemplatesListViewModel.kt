package com.fediphoto.lineage.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import com.fediphoto.lineage.database.TemplateDAO
import com.fediphoto.lineage.datatypes.Template

class TemplatesListViewModel(templateDAO: TemplateDAO) : ViewModel() {
    val templates: LiveData<List<Template>> = templateDAO.getAllFlow().asLiveData()
}


class TemplatesListViewModelFactory(private val templateDAO: TemplateDAO) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TemplatesListViewModel::class.java))
            @Suppress("UNCHECKED_CAST")
            return TemplatesListViewModel(templateDAO) as T

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
