package com.fediphoto.lineage.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import com.fediphoto.lineage.database.AccountDAO
import com.fediphoto.lineage.datatypes.FediAccount

class AccountsListViewModel(accountDAO: AccountDAO) : ViewModel() {
    val accounts: LiveData<List<FediAccount>> = accountDAO.getAllFlow().asLiveData()
}


class AccountsListViewModelFactory(private val accountDAO: AccountDAO) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AccountsListViewModel::class.java))
            @Suppress("UNCHECKED_CAST")
            return AccountsListViewModel(accountDAO) as T

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
