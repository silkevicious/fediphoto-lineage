package com.fediphoto.lineage.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import com.fediphoto.lineage.database.QueueDAO
import com.fediphoto.lineage.datatypes.listitems.QueueItemListItem

class QueueListViewModel(queueDAO: QueueDAO) : ViewModel() {
    val queueItems: LiveData<List<QueueItemListItem>> = queueDAO.getAllFlow().asLiveData()
}


class QueueListViewModelFactory(private val queueDAO: QueueDAO) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(QueueListViewModel::class.java))
            @Suppress("UNCHECKED_CAST")
            return QueueListViewModel(queueDAO) as T

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
