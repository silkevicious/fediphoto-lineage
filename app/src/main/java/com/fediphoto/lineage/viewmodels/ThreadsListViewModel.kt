package com.fediphoto.lineage.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import com.fediphoto.lineage.database.ThreadDAO
import com.fediphoto.lineage.datatypes.listitems.StatusThreadListItem

class ThreadsListViewModel(threadDAO: ThreadDAO) : ViewModel() {
    val threads: LiveData<List<StatusThreadListItem>> = threadDAO.getAllFlow().asLiveData()
}


class ThreadsListViewModelFactory(private val threadDAO: ThreadDAO) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ThreadsListViewModel::class.java))
            @Suppress("UNCHECKED_CAST")
            return ThreadsListViewModel(threadDAO) as T

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
