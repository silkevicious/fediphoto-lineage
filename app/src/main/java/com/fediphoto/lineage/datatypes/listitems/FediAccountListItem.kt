package com.fediphoto.lineage.datatypes.listitems

import com.fediphoto.lineage.datatypes.FediAccount

data class FediAccountListItem(
    val account: FediAccount,
    var isActive: Boolean
)
