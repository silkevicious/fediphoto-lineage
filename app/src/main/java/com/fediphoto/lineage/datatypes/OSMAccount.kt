package com.fediphoto.lineage.datatypes

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OSMAccount(
    val id: String,
    var token: String,
    @SerialName("display_name") var displayName: String,
    var avatar: String?
)
