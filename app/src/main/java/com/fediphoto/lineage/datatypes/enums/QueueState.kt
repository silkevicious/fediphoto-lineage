package com.fediphoto.lineage.datatypes.enums

import androidx.annotation.StringRes
import com.fediphoto.lineage.R

enum class QueueState(@StringRes val stringId: Int) {
    PENDING(R.string.queue_state_pending),
    REMOVING_EXIF(R.string.queue_state_removing_exif_tags),
    COMPRESSING_IMAGE(R.string.queue_state_compressing_image),
    UPLOADING_MEDIA(R.string.queue_state_uploading_media),
    PUBLISHING_POST(R.string.queue_state_publishing_post),
    CREATING_OSM_NOTE(R.string.queue_state_creating_osm_note),
    DONE(R.string.queue_state_done),
    WAITING_TO_RETRY(R.string.queue_state_waiting_to_retry)
}
