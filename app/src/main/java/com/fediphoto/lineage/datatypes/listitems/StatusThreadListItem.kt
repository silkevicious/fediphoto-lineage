package com.fediphoto.lineage.datatypes.listitems

import androidx.room.Embedded
import androidx.room.Relation
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.StatusThread
import com.fediphoto.lineage.datatypes.Template

data class StatusThreadListItem(
    @Embedded val statusThread: StatusThread,
    @Relation(parentColumn = "account_id", entityColumn = "id") val account: FediAccount,
    @Relation(parentColumn = "template_id", entityColumn = "id") val template: Template
)
