package com.fediphoto.lineage.datatypes

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable

@Serializable
@Entity(tableName = "accounts")
data class FediAccount(
    @PrimaryKey(autoGenerate = true) var id: Int,
    var instance: String,
    var token: String,
    val username: String,
    @ColumnInfo(name = "display_name") val displayName: String,
    val avatar: String
)
