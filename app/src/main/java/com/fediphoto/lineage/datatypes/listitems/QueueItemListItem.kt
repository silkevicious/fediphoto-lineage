package com.fediphoto.lineage.datatypes.listitems

import androidx.room.Embedded
import androidx.room.Relation
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.QueueItem

data class QueueItemListItem(
    @Embedded val queueItem: QueueItem,
    @Relation(parentColumn = "account_id", entityColumn = "id") val account: FediAccount?
)
