package com.fediphoto.lineage.datatypes.enums

enum class InstanceType {
    MASTODON,
    PLEROMA,
    FRIENDICA,
    PIXELFED
}
