package com.fediphoto.lineage

import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.FediApplication
import com.fediphoto.lineage.datatypes.Location
import com.fediphoto.lineage.datatypes.OSMAccount
import io.ktor.client.call.body
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.get
import io.ktor.client.request.headers
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.Parameters
import io.ktor.http.contentType
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.encodeToJsonElement

suspend fun fediCreateApp(hostname: String): FediApplication = httpClient.submitForm(
    "https://$hostname/api/v1/apps",
    Parameters.build {
        append("client_name", "FediPhoto-Lineage")
        append("redirect_uris", REDIRECT_URI)
        append("scopes", FEDI_REQUESTED_SCOPES)
        append("website", "https://silkevicious.codeberg.page/fediphoto-lineage.html")
    },
    false
).body()

suspend fun fediGetAccessToken(hostname: String, application: FediApplication, authCode: String): String = httpClient.submitForm(
    url = "https://$hostname/oauth/token",
    formParameters = Parameters.build {
        append("client_id", application.clientId)
        append("client_secret", application.clientSecret)
        append("grant_type", "authorization_code")
        append("redirect_uri", application.redirectUri)
        append("code", authCode)
        append("scope", FEDI_REQUESTED_SCOPES)
    }
).body<JsonObject>()["access_token"].let { it as JsonPrimitive }.content

suspend fun fediGetAccountForLogin(hostname: String, accessToken: String): FediAccount =
    httpClient.get("https://$hostname/api/v1/accounts/verify_credentials") {
        headers { append("Authorization", "Bearer $accessToken") }
    }.body<JsonObject>().let {
        FediAccount(
            0, hostname, accessToken,
            (it["username"] as JsonPrimitive).content,
            (it["display_name"] as JsonPrimitive).content,
            (it["avatar"] as JsonPrimitive).content
        )
    }

suspend fun osmGetAccessToken(authCode: String): String {
    return httpClient.submitForm(
        url = "https://www.openstreetmap.org/oauth2/token",
        formParameters = Parameters.build {
            append("grant_type", "authorization_code")
            append("code", authCode)
            append("redirect_uri", REDIRECT_URI)
            append("client_id", OSM_CLIENT_ID)
        }
    ).body<JsonObject>()["access_token"].let { it as JsonPrimitive }.content
}

suspend fun osmGetAccountDetails(accessToken: String): OSMAccount =
    httpClient.get("https://api.openstreetmap.org/api/0.6/user/details") {
        headers { append("Authorization", "Bearer $accessToken") }
    }.body<JsonObject>().let {
        val user = it["user"] as JsonObject
        OSMAccount(
            (user["id"] as JsonPrimitive).content,
            accessToken,
            (user["display_name"] as JsonPrimitive).content,
            ((user["img"] as JsonObject?)?.get("href") as JsonPrimitive?)?.content
        )
    }

suspend fun osmCreateNote(accessToken: String?, location: Location, noteText: String) =
    httpClient.post("https://api.openstreetmap.org/api/0.6/notes.json") {
        accessToken?.let { headers { append("Authorization", "Bearer $it") } }
        contentType(ContentType.Application.Json)
        setBody(
            JsonObject(
                mapOf(
                    "lat" to Json.encodeToJsonElement(location.latitude),
                    "lon" to Json.encodeToJsonElement(location.longitude),
                    "text" to Json.encodeToJsonElement(noteText)
                )
            )
        )
    }

