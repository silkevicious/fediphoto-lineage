package com.fediphoto.lineage.database

import androidx.annotation.WorkerThread
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.fediphoto.lineage.datatypes.QueueItem
import com.fediphoto.lineage.datatypes.listitems.QueueItemListItem
import kotlinx.coroutines.flow.Flow

@Dao
interface QueueDAO {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insert(queueItem: QueueItem)

    @Update
    suspend fun update(queueItem: QueueItem)

    @Delete
    suspend fun delete(queueItem: QueueItem): Int

    @Query("SELECT * FROM queue")
    suspend fun getAll(): List<QueueItem>

    @Query("SELECT * FROM queue WHERE status_id is null ORDER BY rowId LIMIT 1")
    suspend fun getFirstPendingQueueItem(): QueueItem?

    @Query("DELETE FROM queue WHERE status_id is not null")
    suspend fun cleanup()

    @WorkerThread
    @Transaction
    @Query("SELECT * FROM queue")
    fun getAllFlow(): Flow<List<QueueItemListItem>>
}
