package com.fediphoto.lineage

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.StringRes
import androidx.preference.PreferenceManager
import com.fediphoto.lineage.datatypes.OSMAccount
import com.fediphoto.lineage.datatypes.enums.AfterPostAction
import com.fediphoto.lineage.datatypes.enums.AppTheme
import com.fediphoto.lineage.datatypes.enums.Visibility
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class Prefs(private val context: Context) {
    private val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    private fun getString(@StringRes id: Int) = context.getString(id)

    var theme: String
        get() = preferences.getString(getString(R.string.keyTheme), null) ?: getString(R.string.valueThemeSystem)
        set(value) = preferences.edit().putString(getString(R.string.keyTheme), value).apply()

    var language: String?
        get() = preferences.getString(getString(R.string.keyLanguage), getString(R.string.valueLanguageSystem))
        set(value) = preferences.edit().putString(getString(R.string.keyLanguage), value).apply()

    var introOnStart: Boolean
        get() = preferences.getBoolean(getString(R.string.keyIntroOnStart), true)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyIntroOnStart), value).apply()

    var activeAccountId: Int
        get() = preferences.getInt(getString(R.string.keyActiveAccountId), 0)
        set(value) = preferences.edit().putInt(getString(R.string.keyActiveAccountId), value).apply()

    var activeTemplateId: Int
        get() = preferences.getInt(getString(R.string.keyActiveTemplateId), 0)
        set(value) = preferences.edit().putInt(getString(R.string.keyActiveTemplateId), value).apply()

    var cameraOnStart: Boolean
        get() = preferences.getBoolean(getString(R.string.keyCameraOnStart), false)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyCameraOnStart), value).apply()

    var previewBeforePost: Boolean
        get() = preferences.getBoolean(getString(R.string.keyPreviewBeforePost), true)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyPreviewBeforePost), value).apply()

    var savePhoto: Boolean
        get() = preferences.getBoolean(getString(R.string.keySavePhoto), true)
        set(value) = preferences.edit().putBoolean(getString(R.string.keySavePhoto), value).apply()

    var locationService: Boolean
        get() = preferences.getBoolean(getString(R.string.keyLocationService), false)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyLocationService), value).apply()

    var osmNotes: Boolean
        get() = preferences.getBoolean(getString(R.string.keyOsmNotes), false)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyOsmNotes), value).apply()

    var osmAccount: OSMAccount?
        get() = preferences.getString(KEY_OSM_ACCOUNT, null)?.let { Json.decodeFromString(it) }
        set(value) = preferences.edit().putString(KEY_OSM_ACCOUNT, Json.encodeToString(value)).apply()

    var osmNoteBeforePhoto: Boolean
        get() = preferences.getBoolean(getString(R.string.keyOsmNoteBeforePhoto), false)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyOsmNoteBeforePhoto), value).apply()

    var osmVisibilityOverrideEnabled: Boolean
        get() = preferences.getBoolean(getString(R.string.keyOsmVisibilityOverrideEnabled), false)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyOsmVisibilityOverrideEnabled), value).apply()

    var osmVisibilityOverrideValue: Visibility
        get() = preferences.getString(getString(R.string.keyOsmVisibilityOverrideValue), Visibility.UNLISTED.name)!!.let { Visibility.valueOf(it) }
        set(value) = preferences.edit().putString(getString(R.string.keyOsmVisibilityOverrideValue), value.name).apply()

    var osmDirectLink: Boolean
        get() = preferences.getBoolean(getString(R.string.keyOsmDirectLink), false)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyOsmDirectLink), value).apply()

    var osmApplicationName: Boolean
        get() = preferences.getBoolean(getString(R.string.keyOsmAppName), true)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyOsmAppName), value).apply()

    var compressImage: Boolean
        get() = preferences.getBoolean(getString(R.string.keyCompressImage), false)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyCompressImage), value).apply()

    var imageQuality: Int
        get() = preferences.getInt(getString(R.string.keyCompressImageQuality), context.resources.getInteger(R.integer.compress_quality_default))
        set(value) = preferences.edit().putInt(getString(R.string.keyCompressImageQuality), value).apply()

    var removeExif: Boolean
        get() = preferences.getBoolean(getString(R.string.keyRemoveExif), true)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyRemoveExif), value).apply()

    var canAutoOpenCamera: Boolean
        get() = preferences.getBoolean(getString(R.string.keyRecreatingActivity), false)
        set(value) = preferences.edit().putBoolean(getString(R.string.keyRecreatingActivity), value).apply()

    var v7MigrationDone: Boolean
        get() = preferences.getBoolean(KEY_V7_MIGRATION, false)
        set(value) = preferences.edit().putBoolean(KEY_V7_MIGRATION, value).apply()

    var altTextMigrationDone: Boolean
        get() = preferences.getBoolean(KEY_ALT_TEXT_MIGRATION, false)
        set(value) = preferences.edit().putBoolean(KEY_ALT_TEXT_MIGRATION, value).apply()

    companion object {
        private const val KEY_OSM_ACCOUNT = "osm_account"
        private const val KEY_V7_MIGRATION = "v7_migration"
        private const val KEY_ALT_TEXT_MIGRATION = "alt_text_migration"
    }
}

@Deprecated("Only used for v7 migration")
class OldPrefs(private val context: Context) {
    private val preferences: SharedPreferences =
        context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)

    var theme: AppTheme
        get() = AppTheme.valueOf(preferences.getString(keyTheme, AppTheme.SYSTEM.name)!!)
        set(value) = preferences.edit().putString(keyTheme, value.name).apply()

    var language: String?
        get() = preferences.getString(keyLanguage, null)
        set(value) = preferences.edit().putString(keyLanguage, value).apply()

    var introDisplayed: Boolean
        get() = preferences.getBoolean(keyIntroDisplayed, false)
        set(value) = preferences.edit().putBoolean(keyIntroDisplayed, value).apply()

    var activeAccountId: Int
        get() = preferences.getInt(keyActiveAccountId, -1)
        set(value) = preferences.edit().putInt(keyActiveAccountId, value).apply()

    var activeTemplateId: Int
        get() = preferences.getInt(keyActiveTemplateId, -1)
        set(value) = preferences.edit().putInt(keyActiveTemplateId, value).apply()

    var cameraOnStart: Boolean
        get() = preferences.getBoolean(keyCameraOnStart, false)
        set(value) = preferences.edit().putBoolean(keyCameraOnStart, value).apply()

    var previewBeforePost: Boolean
        get() = preferences.getBoolean(keyPreviewBeforePost, true)
        set(value) = preferences.edit().putBoolean(keyPreviewBeforePost, value).apply()

    var afterPostAction: AfterPostAction
        get() = AfterPostAction.valueOf(preferences.getString(keyAfterPostAction, AfterPostAction.MOVE.name)!!)
        set(value) = preferences.edit().putString(keyAfterPostAction, value.name).apply()

    var locationService: Boolean
        get() = preferences.getBoolean(keyLocationService, false)
        set(value) = preferences.edit().putBoolean(keyLocationService, value).apply()

    private companion object {
        const val keyTheme = "theme"
        const val keyLanguage = "language"
        const val keyIntroDisplayed = "intro_displayed"
        const val keyActiveAccountId = "active_account_id"
        const val keyActiveTemplateId = "active_template_id"
        const val keyCameraOnStart = "camera_on_start"
        const val keyPreviewBeforePost = "preview_before_post"
        const val keyAfterPostAction = "after_post_action"
        const val keyLocationService = "location_service"
    }
}
