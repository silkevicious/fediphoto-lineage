package com.fediphoto.lineage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fediphoto.lineage.databinding.DialogRemoveQueueItemBinding
import com.fediphoto.lineage.databinding.ListItemQueueBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.QueueItem
import com.fediphoto.lineage.datatypes.listitems.QueueItemListItem
import com.fediphoto.lineage.getRemoveConfirmationDialog
import java.io.File

class QueueItemsListAdapter(
    private val onQueueItemCancelConfirmed: (queueItem: QueueItem) -> Unit
) : ListAdapter<QueueItemListItem, QueueItemsListAdapter.QueueItemViewHolder>(QueueItemComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QueueItemViewHolder =
        QueueItemViewHolder(ListItemQueueBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: QueueItemViewHolder, position: Int) {
        val queueItemListItem = getItem(position)
        val account = queueItemListItem.account

        holder.binding.cancel.setOnClickListener { queueItemCancelConfirmationDialog(holder.itemView.context, queueItemListItem.queueItem, account) }
        if (account != null) {
            holder.binding.account.setAccount(account)
            holder.binding.account.isVisible = true
        } else holder.binding.account.isVisible = false
        holder.binding.template.setHideName(true)
        holder.binding.template.setQueueItem(queueItemListItem.queueItem)
        holder.binding.template.setPhoto(File(queueItemListItem.queueItem.photoPath))
        holder.binding.state.setText(queueItemListItem.queueItem.state.stringId)
        if (queueItemListItem.queueItem.error != null) {
            holder.binding.error.isVisible = true
            holder.binding.errorText.text = queueItemListItem.queueItem.error
        } else holder.binding.error.isVisible = false
    }

    inner class QueueItemViewHolder(val binding: ListItemQueueBinding) : RecyclerView.ViewHolder(binding.root)

    class QueueItemComparator : DiffUtil.ItemCallback<QueueItemListItem>() {
        override fun areItemsTheSame(oldItem: QueueItemListItem, newItem: QueueItemListItem): Boolean = oldItem.queueItem.id == newItem.queueItem.id
        override fun areContentsTheSame(oldItem: QueueItemListItem, newItem: QueueItemListItem): Boolean =
            oldItem.queueItem.state == newItem.queueItem.state && oldItem.queueItem.error == newItem.queueItem.error
    }

    private fun queueItemCancelConfirmationDialog(context: Context, queueItem: QueueItem, account: FediAccount?) {
        val dialogBinding = DialogRemoveQueueItemBinding.inflate(LayoutInflater.from(context))
        if (account != null)
            dialogBinding.account.setAccount(account)
        else
            dialogBinding.account.isVisible = false
        dialogBinding.template.setQueueItem(queueItem)
        getRemoveConfirmationDialog(context, dialogBinding.root) { onQueueItemCancelConfirmed(queueItem) }.show()
    }
}
