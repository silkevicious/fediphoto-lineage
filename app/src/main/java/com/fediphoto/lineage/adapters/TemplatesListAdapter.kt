package com.fediphoto.lineage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fediphoto.lineage.R
import com.fediphoto.lineage.databinding.DialogRemoveTemplateBinding
import com.fediphoto.lineage.databinding.ListItemTemplatesBinding
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.eiffelTowerLocation
import com.fediphoto.lineage.getRemoveConfirmationDialog
import java.util.Date

class TemplatesListAdapter(
    private val onTemplateEditPressed: (templateId: Int) -> Unit,
    private val onTemplateCopyPressed: (template: Template) -> Unit,
    private val onTemplateActivated: (templateId: Int) -> Unit,
    private val onTemplateDeleteConfirmed: (template: Template) -> Unit,
) : ListAdapter<Template, TemplatesListAdapter.TemplateViewHolder>(TemplateComparator()) {

    private val previewDate = Date()
    var activeTemplateId: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TemplateViewHolder =
        TemplateViewHolder(ListItemTemplatesBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: TemplateViewHolder, position: Int) {
        val template = getItem(position)
        holder.binding.edit.setOnClickListener { onTemplateEditPressed(template.id) }
        holder.binding.delete.setOnClickListener { onDeletePressed(holder.binding.root.context, template) }
        holder.binding.copy.setOnClickListener { onTemplateCopyPressed(template) }
        holder.binding.template.setTemplate(template, previewDate, eiffelTowerLocation)
        holder.binding.activeIndicator.setOnClickListener { onTemplateActivated(template.id) }
        holder.binding.template.setOnClickListener { onTemplateActivated(template.id) }
        holder.binding.activeIndicator.setImageResource(
            if (template.id == activeTemplateId)
                R.drawable.ic_radio_button_checked
            else
                R.drawable.ic_radio_button_unchecked
        )
    }

    inner class TemplateViewHolder(val binding: ListItemTemplatesBinding) : RecyclerView.ViewHolder(binding.root)

    class TemplateComparator : DiffUtil.ItemCallback<Template>() {
        override fun areItemsTheSame(oldItem: Template, newItem: Template): Boolean = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Template, newItem: Template): Boolean =
            oldItem.name == newItem.name &&
                    oldItem.spoilerText == newItem.spoilerText &&
                    oldItem.spoilerEnabled == newItem.spoilerEnabled &&
                    oldItem.text == newItem.text &&
                    oldItem.sensitiveMedia == newItem.sensitiveMedia &&
                    oldItem.visibility == newItem.visibility &&
                    oldItem.threading == newItem.threading &&
                    oldItem.date == newItem.date &&
                    oldItem.dateFormat == newItem.dateFormat &&
                    oldItem.location == newItem.location &&
                    oldItem.locationFormat == newItem.locationFormat
    }

    private fun onDeletePressed(context: Context, template: Template) {
        val dialogBinding = DialogRemoveTemplateBinding.inflate(LayoutInflater.from(context))
        dialogBinding.template.setTemplate(template, previewDate, eiffelTowerLocation)
        getRemoveConfirmationDialog(context, dialogBinding.root) { onTemplateDeleteConfirmed(template) }.show()
    }
}
