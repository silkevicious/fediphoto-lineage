package com.fediphoto.lineage.adapters

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.fediphoto.lineage.R

class RecyclerViewItemSpacing(context: Context, orientation: Int) : DividerItemDecoration(context, orientation) {
    private val space = context.resources.getDimensionPixelSize(R.dimen.space_mid) / 2
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.bottom = space
        outRect.top = space
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        /* Keep empty to remove divider */
    }
}
