package com.fediphoto.lineage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fediphoto.lineage.R
import com.fediphoto.lineage.databinding.DialogRemoveAccountBinding
import com.fediphoto.lineage.databinding.ListItemAccountsBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.listitems.FediAccountListItem
import com.fediphoto.lineage.getRemoveConfirmationDialog

class AccountsListAdapter(
    private val onAccountActivated: (accountId: Int) -> Unit,
    private val onAccountRemoveConfirmed: (account: FediAccount) -> Unit,
) : ListAdapter<FediAccountListItem, AccountsListAdapter.FediAccountViewHolder>(FediAccountComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountsListAdapter.FediAccountViewHolder =
        FediAccountViewHolder(ListItemAccountsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: AccountsListAdapter.FediAccountViewHolder, position: Int) {
        val fediAccountListItem = getItem(position)

        holder.binding.delete.setOnClickListener { accountRemoveConfirmationDialog(holder.binding.root.context, fediAccountListItem.account) }
        holder.binding.account.setAccount(fediAccountListItem.account)
        holder.binding.account.setOnClickListener { onAccountActivated(fediAccountListItem.account.id) }
        holder.binding.activeIndicator.setOnClickListener { onAccountActivated(fediAccountListItem.account.id) }
        holder.binding.activeIndicator.setImageResource(
            if (fediAccountListItem.isActive)
                R.drawable.ic_radio_button_checked
            else
                R.drawable.ic_radio_button_unchecked
        )
    }

    inner class FediAccountViewHolder(val binding: ListItemAccountsBinding) : RecyclerView.ViewHolder(binding.root)

    class FediAccountComparator : DiffUtil.ItemCallback<FediAccountListItem>() {
        override fun areItemsTheSame(oldItem: FediAccountListItem, newItem: FediAccountListItem): Boolean = oldItem.account.id == newItem.account.id
        override fun areContentsTheSame(oldItem: FediAccountListItem, newItem: FediAccountListItem): Boolean =
            oldItem.account.avatar == newItem.account.avatar &&
                    oldItem.account.displayName == newItem.account.displayName &&
                    oldItem.account.username == newItem.account.username &&
                    oldItem.isActive == newItem.isActive
    }

    private fun accountRemoveConfirmationDialog(context: Context, account: FediAccount) {
        val dialogBinding = DialogRemoveAccountBinding.inflate(LayoutInflater.from(context))
        dialogBinding.account.setAccount(account)
        getRemoveConfirmationDialog(context, dialogBinding.root) { onAccountRemoveConfirmed(account) }.show()
    }
}
