package com.fediphoto.lineage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fediphoto.lineage.databinding.DialogRemoveThreadBinding
import com.fediphoto.lineage.databinding.DialogTemplatePreviewBinding
import com.fediphoto.lineage.databinding.ListItemThreadsBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.StatusThread
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.datatypes.listitems.StatusThreadListItem
import com.fediphoto.lineage.eiffelTowerLocation
import com.fediphoto.lineage.getRemoveConfirmationDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.Date

class ThreadsListAdapter(
    private val activeAccountId: Int,
    private val activeTemplateId: Int,
    private val onThreadDeleteConfirmed: (thread: StatusThread) -> Unit
) : ListAdapter<StatusThreadListItem, ThreadsListAdapter.StatusThreadViewHolder>(StatusThreadComparator()) {
    private val previewDate = Date()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatusThreadViewHolder =
        StatusThreadViewHolder(ListItemThreadsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: StatusThreadViewHolder, position: Int) {
        val statusThreadListItem = getItem(position)
        val context = holder.itemView.context

        holder.binding.activeIndicator.isVisible =
            statusThreadListItem.account.id == activeAccountId && statusThreadListItem.template.id == activeTemplateId
        holder.binding.account.setAccount(statusThreadListItem.account)
        holder.binding.templateName.text = statusThreadListItem.template.name
        holder.binding.templateName.setOnClickListener { showTemplatePreview(context, statusThreadListItem.template) }
        holder.binding.lastPostedOn.text = formatThreadDate(statusThreadListItem.statusThread.statusDate)

        holder.binding.delete.setOnClickListener {
            threadRemoveConfirmationDialog(context, statusThreadListItem.statusThread, statusThreadListItem.account, statusThreadListItem.template)
        }
    }

    inner class StatusThreadViewHolder(val binding: ListItemThreadsBinding) : RecyclerView.ViewHolder(binding.root)

    class StatusThreadComparator : DiffUtil.ItemCallback<StatusThreadListItem>() {
        override fun areItemsTheSame(oldItem: StatusThreadListItem, newItem: StatusThreadListItem): Boolean = oldItem == newItem
        override fun areContentsTheSame(oldItem: StatusThreadListItem, newItem: StatusThreadListItem): Boolean =
            oldItem.statusThread.accountId == newItem.statusThread.accountId && oldItem.statusThread.templateId == newItem.statusThread.templateId
    }

    private fun showTemplatePreview(context: Context, template: Template) {
        val dialog = MaterialAlertDialogBuilder(context).create()
        val templatePreviewDialogBinding = DialogTemplatePreviewBinding.inflate(LayoutInflater.from(dialog.context))
        templatePreviewDialogBinding.templateView.setTemplate(template, previewDate, eiffelTowerLocation)
        templatePreviewDialogBinding.button.setOnClickListener { dialog.dismiss() }
        dialog.setView(templatePreviewDialogBinding.root)
        dialog.show()
    }

    private fun threadRemoveConfirmationDialog(context: Context, thread: StatusThread, account: FediAccount, template: Template) {
        val dialogViewBinding = DialogRemoveThreadBinding.inflate(LayoutInflater.from(context))
        val dialog = getRemoveConfirmationDialog(context, dialogViewBinding.root) { onThreadDeleteConfirmed(thread) }

        dialogViewBinding.account.setAccount(account)
        dialogViewBinding.templateName.text = template.name
        dialogViewBinding.templateName.setOnClickListener { showTemplatePreview(context, template) }
        dialogViewBinding.lastPostedOn.text = formatThreadDate(thread.statusDate)

        dialog.show()
    }

    private fun formatThreadDate(statusDate: String) = StringBuilder(statusDate).insert(6, "-").insert(4, "-")
}
