package com.fediphoto.lineage

import android.app.Application
import com.fediphoto.lineage.database.FPLData

class FPLApplication : Application() {
    val fplData by lazy { FPLData.getInstance(this) }
    val prefs by lazy { Prefs(this) }
}
