package com.fediphoto.lineage.fragments.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.AccountDAO
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.DialogRemoveAccountBinding
import com.fediphoto.lineage.databinding.FragmentIntroAccountBinding
import com.fediphoto.lineage.getRemoveConfirmationDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AccountIntroFragment : Fragment() {
    private var _binding: FragmentIntroAccountBinding? = null
    private val binding: FragmentIntroAccountBinding get() = _binding!!

    private val accountDAO: AccountDAO by lazy { FPLData.getInstance(requireActivity().applicationContext).getAccountDAO() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentIntroAccountBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.add.setOnClickListener { findNavController().navigate(AccountIntroFragmentDirections.accountToLogin()) }
        binding.nav.previous.setOnClickListener { findNavController().popBackStack() }
        binding.nav.next.setOnClickListener { findNavController().navigate(AccountIntroFragmentDirections.accountToTemplate()) }

        updateViews()
    }

    private fun updateViews() {
        lifecycleScope.launch(Dispatchers.Main) {
            val account = accountDAO.getById(Prefs(requireContext()).activeAccountId)
            if (account == null) {
                binding.accountContainer.root.isVisible = false
                binding.add.isVisible = true
                binding.addMoreLater.isVisible = false
                binding.nav.next.setText(R.string.nav_skip)
            } else {
                binding.accountContainer.account.setAccount(account)
                binding.accountContainer.activeIndicator.setImageResource(R.drawable.ic_radio_button_checked)
                binding.accountContainer.delete.setOnClickListener {
                    val dialogBinding = DialogRemoveAccountBinding.inflate(LayoutInflater.from(requireContext()))
                    dialogBinding.account.setAccount(account)
                    getRemoveConfirmationDialog(requireContext(), dialogBinding.root) {
                        lifecycleScope.launch(Dispatchers.Main) {
                            accountDAO.delete(account)
                            updateViews()
                        }
                    }.show()
                }
                binding.accountContainer.root.isVisible = true
                binding.addMoreLater.isVisible = true
                binding.add.isVisible = false
                binding.nav.next.setText(R.string.nav_next)
            }
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden) updateViews()
        super.onHiddenChanged(hidden)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
