package com.fediphoto.lineage.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.fediphoto.lineage.BuildConfig
import com.fediphoto.lineage.IntroActivity
import com.fediphoto.lineage.R
import com.fediphoto.lineage.databinding.FragmentAboutBinding

class AboutFragment : Fragment() {
    private var _binding: FragmentAboutBinding? = null
    private val binding: FragmentAboutBinding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentAboutBinding.inflate(inflater, container, false)
        binding.logo.setOnClickListener { openUrl(getString(R.string.url_website)) }
        @SuppressLint("SetTextI18n")
        binding.version.text = "v${BuildConfig.VERSION_NAME}"
        binding.changelog.setOnClickListener { openUrl(getString(R.string.url_changelog)) }
        binding.help.setOnClickListener { startActivity(Intent(requireActivity(), IntroActivity::class.java)) }
        binding.issues.setOnClickListener { openUrl(getString(R.string.url_issues)) }
        binding.fedi.setOnClickListener { openUrl(getString(R.string.url_fedi)) }
        binding.license.setOnClickListener { openUrl(getString(R.string.url_license)) }
        return binding.root
    }

    private fun openUrl(url: String) = startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
