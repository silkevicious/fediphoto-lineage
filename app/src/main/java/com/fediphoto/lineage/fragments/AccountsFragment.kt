package com.fediphoto.lineage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.adapters.AccountsListAdapter
import com.fediphoto.lineage.adapters.RecyclerViewItemSpacing
import com.fediphoto.lineage.databinding.FragmentListBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.listitems.FediAccountListItem
import com.fediphoto.lineage.fplAccountDAO
import com.fediphoto.lineage.fplPrefs
import com.fediphoto.lineage.viewmodels.AccountsListViewModel
import com.fediphoto.lineage.viewmodels.AccountsListViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AccountsFragment : Fragment() {
    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!

    private val accountsListViewModel: AccountsListViewModel by viewModels { AccountsListViewModelFactory(fplAccountDAO()) }
    private val prefs: Prefs by lazy { Prefs(requireActivity().applicationContext) }
    private val adapter: AccountsListAdapter get() = binding.list.adapter as AccountsListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.setHasFixedSize(true)
        binding.list.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.list.addItemDecoration(RecyclerViewItemSpacing(binding.list.context, DividerItemDecoration.VERTICAL))
        binding.button.text = getString(R.string.add_account)
        binding.button.setOnClickListener { findNavController().navigate(AccountsFragmentDirections.accountsToLogin()) }

        binding.list.adapter = AccountsListAdapter(this::onAccountActivated, this::onAccountRemoveConfirmed)
        accountsListViewModel.accounts.observe(viewLifecycleOwner) { accountsList ->
            adapter.submitList(accountsList.toFediAccountListItemList())
        }
    }

    private fun List<FediAccount>.toFediAccountListItemList(): List<FediAccountListItem> =
        map { FediAccountListItem(it, it.id == prefs.activeAccountId) }

    private fun onAccountActivated(accountId: Int) {
        if (prefs.activeAccountId == accountId) return

        val oldActiveIndex = adapter.currentList.indexOfFirst { it.isActive }
        val newActiveIndex = adapter.currentList.indexOfFirst { it.account.id == accountId }

        prefs.activeAccountId = accountId

        adapter.currentList[oldActiveIndex].isActive = false
        adapter.currentList[newActiveIndex].isActive = true

        adapter.notifyItemChanged(oldActiveIndex)
        adapter.notifyItemChanged(newActiveIndex)
    }

    private fun onAccountRemoveConfirmed(account: FediAccount) {
        lifecycleScope.launch(Dispatchers.IO) {
            if (adapter.currentList.size > 1) {
                val oldIndex = adapter.currentList.indexOfFirst { it.account.id == account.id }
                val newIndex =
                    if (oldIndex < adapter.currentList.lastIndex)
                        oldIndex + 1
                    else
                        oldIndex - 1
                fplPrefs.activeAccountId = adapter.currentList[newIndex].account.id
            } else fplPrefs.activeTemplateId = 0

            fplAccountDAO().delete(account)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
