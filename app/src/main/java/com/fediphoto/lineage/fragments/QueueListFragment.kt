package com.fediphoto.lineage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.fediphoto.lineage.adapters.QueueItemsListAdapter
import com.fediphoto.lineage.adapters.RecyclerViewItemSpacing
import com.fediphoto.lineage.databinding.FragmentListBinding
import com.fediphoto.lineage.fplQueueDAO
import com.fediphoto.lineage.viewmodels.QueueListViewModel
import com.fediphoto.lineage.viewmodels.QueueListViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class QueueListFragment : Fragment() {
    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!

    private val queueListViewModel: QueueListViewModel by viewModels { QueueListViewModelFactory(fplQueueDAO()) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.button.isVisible = false
        binding.list.setHasFixedSize(true)
        binding.list.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.list.addItemDecoration(RecyclerViewItemSpacing(binding.list.context, DividerItemDecoration.VERTICAL))
        val adapter = QueueItemsListAdapter {
            lifecycleScope.launch(Dispatchers.IO) { fplQueueDAO().delete(it) }
        }
        binding.list.adapter = adapter

        queueListViewModel.queueItems.observe(viewLifecycleOwner) { queueItemsList ->
            queueItemsList.let { adapter.submitList(it) }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
