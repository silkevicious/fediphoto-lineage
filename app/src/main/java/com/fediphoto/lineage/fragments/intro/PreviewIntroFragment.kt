package com.fediphoto.lineage.fragments.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.databinding.FragmentIntroPreviewBinding

class PreviewIntroFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentIntroPreviewBinding.inflate(inflater, container, false)
        val prefs = Prefs(requireContext())

        binding.nav.previous.setOnClickListener { findNavController().popBackStack() }
        binding.nav.next.setOnClickListener { findNavController().navigate(PreviewIntroFragmentDirections.previewToDone()) }

        binding.previewSwitch.isChecked = prefs.previewBeforePost
        binding.previewSwitch.setOnCheckedChangeListener { _, isChecked -> prefs.previewBeforePost = isChecked }

        return binding.root
    }
}
