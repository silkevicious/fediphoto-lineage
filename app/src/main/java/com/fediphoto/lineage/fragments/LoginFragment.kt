package com.fediphoto.lineage.fragments

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.webkit.CookieManager
import android.webkit.WebStorage
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.fediphoto.lineage.FEDI_REQUESTED_SCOPES
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.FragmentLoginBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.FediApplication
import com.fediphoto.lineage.fediCreateApp
import com.fediphoto.lineage.fediGetAccessToken
import com.fediphoto.lineage.fediGetAccountForLogin
import com.fediphoto.lineage.showErrorDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding: FragmentLoginBinding get() = _binding!!

    private lateinit var webView: WebView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.instanceHostname.doOnTextChanged { _, _, _, _ -> binding.instanceHostname.error = null }

        binding.buttonContinue.setOnClickListener {
            setDisplayLoadingAnimation(true)
            (context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                .hideSoftInputFromWindow(binding.instanceHostname.windowToken, 0)
            lifecycleScope.launch(Dispatchers.IO) {
                val hostname = binding.instanceHostname.text?.removePrefix("https://")?.removePrefix("http://").toString()
                if (Patterns.WEB_URL.matcher(hostname).matches())
                    createApp(hostname)
                else
                    withContext(Dispatchers.Main) {
                        setDisplayLoadingAnimation(false)
                        binding.instanceHostname.error = getString(R.string.invalid_domain)
                    }
            }
        }
    }

    private suspend fun createApp(hostName: String) {
        kotlin.runCatching { fediCreateApp(hostName) }
            .onSuccess { withContext(Dispatchers.Main) { getAuthorizationCode(hostName, it) } }
            .onFailure {
                withContext(Dispatchers.Main) {
                    showErrorDialog(requireContext(), it.localizedMessage)
                    setDisplayLoadingAnimation(false)
                    it.printStackTrace()
                }
            }
    }

    private fun getAuthorizationCode(hostname: String, app: FediApplication) {
        binding.webViewStub.setOnInflateListener { _, inflated: View -> webView = inflated as WebView }
        binding.webViewStub.isVisible = true
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                view.isVisible = url.startsWith("http")
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView, url: String) {
                view.isVisible = url.startsWith("http")
                if (url.startsWith(app.redirectUri)) {
                    webView.isVisible = false
                    setDisplayLoadingAnimation(true)
                    lifecycleScope.launch(Dispatchers.IO) {
                        kotlin.runCatching { Uri.parse(url).getQueryParameter("code") ?: throw Exception("Authentication parameter not found") }
                            .onSuccess { getAccessToken(hostname, app, it) }
                            .onFailure {
                                withContext(Dispatchers.Main) {
                                    showErrorDialog(requireContext(), it.localizedMessage)
                                    setDisplayLoadingAnimation(false)
                                    it.printStackTrace()
                                }
                            }
                    }
                }
                super.onPageFinished(view, url)
            }
        }

        webView.loadUrl("https://$hostname/oauth/authorize?client_id=${app.clientId}&scope=$FEDI_REQUESTED_SCOPES&redirect_uri=${app.redirectUri}&response_type=code")
    }

    private suspend fun getAccessToken(hostname: String, application: FediApplication, authCode: String) {
        kotlin.runCatching { fediGetAccessToken(hostname, application, authCode) }
            .onSuccess { getAccount(hostname, it) }
            .onFailure {
                withContext(Dispatchers.Main) {
                    showErrorDialog(requireContext(), it.localizedMessage)
                    setDisplayLoadingAnimation(false)
                    it.printStackTrace()
                }
            }
    }

    private suspend fun getAccount(hostname: String, accessToken: String) {
        kotlin.runCatching { fediGetAccountForLogin(hostname, accessToken) }
            .onSuccess { saveAccount(it) }
            .onFailure {
                withContext(Dispatchers.Main) {
                    showErrorDialog(requireContext(), it.localizedMessage)
                    setDisplayLoadingAnimation(false)
                    it.printStackTrace()
                }
            }
    }

    private suspend fun saveAccount(newAccount: FediAccount) {
        kotlin.runCatching {
            val accountDao = FPLData.getInstance(requireActivity().applicationContext).getAccountDAO()
            val rowId = accountDao.insert(newAccount)
            val account = accountDao.getByRowId(rowId)
            Prefs(requireContext()).activeAccountId = account.id
            withContext(Dispatchers.Main) { clearData() }
        }.onSuccess {
            withContext(Dispatchers.Main) { findNavController().popBackStack() }
        }.onFailure {
            withContext(Dispatchers.Main) {
                showErrorDialog(requireContext(), it.localizedMessage)
                setDisplayLoadingAnimation(false)
                it.printStackTrace()
            }
        }
    }

    private fun clearData() {
        WebStorage.getInstance().deleteAllData()
        CookieManager.getInstance().apply {
            removeAllCookies(null)
            flush()
        }
        webView.apply {
            clearCache(true)
            clearHistory()
            clearFormData()
            clearSslPreferences()
        }
    }

    private fun setDisplayLoadingAnimation(display: Boolean) {
        binding.progress.isVisible = display
        binding.buttonContinue.isVisible = !display
        binding.userInput.isEnabled = !display
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
