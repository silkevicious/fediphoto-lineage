package com.fediphoto.lineage.fragments.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.fediphoto.lineage.databinding.FragmentIntroWelcomeBinding

class WelcomeIntroFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val viewBinding = FragmentIntroWelcomeBinding.inflate(inflater, container, false)
        viewBinding.nav.previous.isVisible = false
        viewBinding.nav.next.setOnClickListener { findNavController().navigate(WelcomeIntroFragmentDirections.welcomeToPermission()) }
        return viewBinding.root
    }
}
