package com.fediphoto.lineage.fragments.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.database.TemplateDAO
import com.fediphoto.lineage.databinding.DialogRemoveTemplateBinding
import com.fediphoto.lineage.databinding.FragmentIntroTemplateBinding
import com.fediphoto.lineage.eiffelTowerLocation
import com.fediphoto.lineage.getRemoveConfirmationDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Date

class TemplateIntroFragment : Fragment() {
    private var _binding: FragmentIntroTemplateBinding? = null
    private val binding: FragmentIntroTemplateBinding get() = _binding!!

    private val templateDAO: TemplateDAO by lazy { FPLData.getInstance(requireActivity().applicationContext).getTemplateDAO() }

    private val previewDate = Date()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentIntroTemplateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.add.setOnClickListener { findNavController().navigate(TemplateIntroFragmentDirections.templateToTemplate()) }
        binding.nav.previous.setOnClickListener { findNavController().popBackStack() }
        binding.nav.next.setOnClickListener { findNavController().navigate(TemplateIntroFragmentDirections.templateToPreview()) }

        updateViews()
    }

    private fun updateViews() {
        lifecycleScope.launch(Dispatchers.IO) {
            val template = templateDAO.getById(Prefs(requireContext()).activeTemplateId)
            if (template == null) withContext(Dispatchers.Main) {
                binding.templateContainer.root.isVisible = false
                binding.add.isVisible = true
                binding.addMoreLater.isVisible = false
                binding.nav.next.setText(R.string.nav_skip)
            } else withContext(Dispatchers.Main) {
                binding.templateContainer.template.setTemplate(template, previewDate, eiffelTowerLocation)
                binding.templateContainer.activeIndicator.setImageResource(R.drawable.ic_radio_button_checked)
                binding.templateContainer.delete.setOnClickListener {
                    val dialogBinding = DialogRemoveTemplateBinding.inflate(LayoutInflater.from(requireContext()))
                    dialogBinding.template.setTemplate(template, previewDate, eiffelTowerLocation)
                    getRemoveConfirmationDialog(requireContext(), dialogBinding.root) {
                        lifecycleScope.launch(Dispatchers.Main) {
                            templateDAO.delete(template)
                            updateViews()
                        }
                    }.show()
                }
                binding.templateContainer.edit.setOnClickListener {
                    findNavController().navigate(TemplateIntroFragmentDirections.templateToTemplate(template.id))
                }
                binding.templateContainer.copy.isVisible = false
                binding.templateContainer.root.isVisible = true
                binding.addMoreLater.isVisible = true
                binding.add.isVisible = false
                binding.nav.next.setText(R.string.nav_next)
            }
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden) updateViews()
        super.onHiddenChanged(hidden)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
