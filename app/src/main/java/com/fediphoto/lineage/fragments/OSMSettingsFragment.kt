package com.fediphoto.lineage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.DrawableRes
import androidx.navigation.fragment.findNavController
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.databinding.DialogOsmAccountBinding
import com.fediphoto.lineage.databinding.DialogRemoveAccountBinding
import com.fediphoto.lineage.datatypes.OSMAccount
import com.fediphoto.lineage.datatypes.enums.Visibility
import com.fediphoto.lineage.getRemoveConfirmationDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class OSMSettingsFragment : PreferenceFragmentCompat() {
    private val prefs: Prefs by lazy { Prefs(requireActivity().applicationContext) }
    private val osmAccountPreference: Preference? by lazy { findPreference(getString(R.string.keyOSMAccount)) }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.osm_preferences, rootKey)
        osmAccountPreference?.setOnPreferenceClickListener {
            prefs.osmAccount.let { osmAccount ->
                if (osmAccount == null)
                    findNavController().navigate(OSMSettingsFragmentDirections.osmToOsmLogin())
                else
                    osmAccountPreview(osmAccount)
            }
            false
        }
        val overrideVisibilityMap = mapOf(
            Visibility.PUBLIC to getString(R.string.visibility_public),
            Visibility.UNLISTED to getString(R.string.visibility_unlisted)
        )
        val overrideVisibilityValuePreference = findPreference<ListPreference>(getString(R.string.keyOsmVisibilityOverrideValue))
        overrideVisibilityValuePreference?.entries = overrideVisibilityMap.keys.map { getString(it.stringId) }.toTypedArray()
        overrideVisibilityValuePreference?.entryValues = overrideVisibilityMap.keys.map { it.name }.toTypedArray()
        overrideVisibilityValuePreference?.setDefaultValue(Visibility.UNLISTED.name)
        overrideVisibilityValuePreference?.summaryProvider = Preference.SummaryProvider<ListPreference> { preference ->
            overrideVisibilityMap.keys.first { visibility -> visibility.name == preference.value }.stringId.let { stringId -> getString(stringId) }
        }
        overrideVisibilityValuePreference?.setOnPreferenceChangeListener { preference, newValue ->
            preference.setIcon(getVisibilityIcon(Visibility.valueOf(newValue as String)))
            true
        }
        overrideVisibilityValuePreference?.setIcon(getVisibilityIcon(Visibility.valueOf(overrideVisibilityValuePreference.value)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateAccountPrefView()
    }

    private fun osmAccountPreview(osmAccount: OSMAccount) {
        val dialogBinding = DialogOsmAccountBinding.inflate(LayoutInflater.from(requireContext()))
        val dialog = MaterialAlertDialogBuilder(requireContext()).setView(dialogBinding.root).create()
        dialogBinding.remove.setOnClickListener {
            onAccountRemovePressed(osmAccount)
            dialog.dismiss()
        }
        dialogBinding.ok.setOnClickListener { dialog.dismiss() }
        dialogBinding.account.setAccount(osmAccount)
        dialog.show()
    }

    private fun onAccountRemovePressed(osmAccount: OSMAccount) {
        val dialogBinding = DialogRemoveAccountBinding.inflate(LayoutInflater.from(requireContext()))
        dialogBinding.account.setAccount(osmAccount)
        getRemoveConfirmationDialog(requireContext(), dialogBinding.root) {
            prefs.osmAccount = null
            updateAccountPrefView()
        }.show()
    }

    private fun updateAccountPrefView() {
        osmAccountPreference?.summary = prefs.osmAccount?.displayName ?: getString(R.string.add_account)
    }

    @DrawableRes
    private fun getVisibilityIcon(value: Visibility): Int = when (value) {
        Visibility.PUBLIC -> R.drawable.ic_public
        Visibility.UNLISTED -> R.drawable.ic_unlisted
        Visibility.PRIVATE -> R.drawable.ic_followers
        Visibility.DIRECT -> R.drawable.ic_direct
    }
}
