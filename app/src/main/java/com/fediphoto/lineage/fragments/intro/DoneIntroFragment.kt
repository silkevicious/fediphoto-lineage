package com.fediphoto.lineage.fragments.intro

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.fediphoto.lineage.MainActivity
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.databinding.FragmentIntroDoneBinding

class DoneIntroFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentIntroDoneBinding.inflate(inflater, container, false)
        binding.nav.previous.setOnClickListener { findNavController().popBackStack() }
        binding.nav.next.text = getString(R.string.nav_finish)
        binding.nav.next.setOnClickListener {
            Prefs(requireContext()).introOnStart = false
            startActivity(Intent(requireActivity(), MainActivity::class.java))
            requireActivity().finish()
        }
        return binding.root
    }
}
