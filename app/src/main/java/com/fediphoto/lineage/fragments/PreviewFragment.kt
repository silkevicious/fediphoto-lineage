package com.fediphoto.lineage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.MenuProvider
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.createContent
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.DialogVisibilityBinding
import com.fediphoto.lineage.databinding.FragmentPreviewBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.Location
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.datatypes.enums.Visibility
import com.fediphoto.lineage.submitWorkerPost
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import java.io.File
import java.util.Date

class PreviewFragment : Fragment() {
    private var _binding: FragmentPreviewBinding? = null
    private val binding: FragmentPreviewBinding get() = _binding!!

    private val prefs: Prefs by lazy { Prefs(requireActivity().applicationContext) }
    private val fplData: FPLData by lazy { FPLData.getInstance(requireActivity().applicationContext) }
    private val args: PreviewFragmentArgs by navArgs()

    private lateinit var account: FediAccount
    private lateinit var template: Template
    private lateinit var date: Date
    private var location: Location? = null
    private var contentFromTemplate: String = ""

    private var editMode: Boolean = false
    private var editModeInitialized: Boolean = false

    private var modifiedSpoilerText: String? = null
    private var modifiedSpoilerEnabled: Boolean? = null
    private var modifiedContent: String? = null
    private var modifiedContentDescription: String? = null
    private var modifiedContentDescriptionEnabled: Boolean? = null
    private var modifiedSensitiveMedia: Boolean? = null
    private var modifiedVisibility: Visibility? = null
    private var newThread: Boolean = false

    private val spoilerEnabled: Boolean get() = modifiedSpoilerEnabled ?: template.spoilerEnabled
    private val spoilerText: String get() = modifiedSpoilerText ?: template.spoilerText
    private val content: String get() = modifiedContent ?: contentFromTemplate
    private val contentDescription: String
        get() = modifiedContentDescription ?: template.defaultContentDescription.takeIf { template.defaultContentDescriptionEnabled } ?: ""
    private val contentDescriptionEnabled: Boolean get() = modifiedContentDescriptionEnabled ?: template.defaultContentDescriptionEnabled
    private val sensitiveMedia: Boolean get() = modifiedSensitiveMedia ?: template.sensitiveMedia
    private val visibility: Visibility get() = modifiedVisibility ?: template.visibility

    private var visibilityDialog: AlertDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentPreviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) = menuInflater.inflate(R.menu.menu_preview, menu)

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean = when (menuItem.itemId) {
                R.id.action_edit -> {
                    switchToEditMode()
                    activity?.invalidateOptionsMenu()
                    true
                }

                R.id.action_undo -> {
                    setInitialValues()
                    true
                }

                R.id.action_save -> {
                    switchToPreviewMode()
                    activity?.invalidateOptionsMenu()
                    true
                }

                else -> false
            }

            override fun onPrepareMenu(menu: Menu) {
                menu.findItem(R.id.action_edit)?.isVisible = !editMode
                menu.findItem(R.id.action_undo)?.isVisible = editMode
                menu.findItem(R.id.action_save)?.isVisible = editMode

                super.onPrepareMenu(menu)
            }
        }, viewLifecycleOwner)

        lifecycleScope.launch(Dispatchers.IO) {
            account = fplData.getAccountDAO().getById(prefs.activeAccountId)!!
            template = fplData.getTemplateDAO().getById(prefs.activeTemplateId)!!
            val photoPath: String = args.photoPath
            val photoFile = File(photoPath)
            date = photoFile.lastModified().let { lastModified -> if (lastModified != 0L) Date(lastModified) else Date() }
            location = args.location?.let { Json.decodeFromString(it) }
            contentFromTemplate = createContent(template, date, location)

            withContext(Dispatchers.Main) {
                setInitialValues()
                switchToPreviewMode()
                binding.account.setAccount(account)
                binding.threading.setText(template.threading.stringId)
                binding.photo.load(photoFile)
                binding.publish.setOnClickListener {
                    lifecycleScope.launch(Dispatchers.Main) {
                        submitWorkerPost(
                            context = requireContext(),
                            activeAccount = account,
                            activeTemplate = template,
                            queueDAO = fplData.getQueueDAO(),
                            photoPath = photoPath,
                            location = location,
                            modifiedSpoilerText = modifiedSpoilerText,
                            modifiedSpoilerEnabled = modifiedSpoilerEnabled,
                            modifiedContent = modifiedContent,
                            contentDescription = contentDescription,
                            modifiedSensitiveMedia = modifiedSensitiveMedia,
                            modifiedVisibility = modifiedVisibility,
                            removeExif = prefs.removeExif,
                            compressImage = prefs.compressImage,
                            compressImageQuality = prefs.imageQuality,
                            osmToken = prefs.osmAccount?.token,
                            osmNoteText = args.osmNote,
                            osmOverrideVisibility = prefs.osmVisibilityOverrideValue.takeIf { prefs.osmVisibilityOverrideEnabled })
                        findNavController().navigate(PreviewFragmentDirections.previewToHome())
                    }
                }
                binding.cancel.setOnClickListener { findNavController().navigate(PreviewFragmentDirections.previewToHome()) }
            }
        }
    }

    private fun setInitialValues() {
        modifiedSpoilerText = null
        modifiedSpoilerEnabled = null
        modifiedContent = null
        modifiedContentDescription = null
        modifiedContentDescriptionEnabled = null
        modifiedSensitiveMedia = null
        modifiedVisibility = null
        newThread = false

        binding.contentWarning.setText(spoilerText)
        binding.spoilerSwitch.isChecked = spoilerEnabled
        binding.content.setText(content)
        binding.contentDescription.setText(template.defaultContentDescription)
        binding.contentDescriptionSwitch.isChecked = contentDescriptionEnabled
        binding.sensitiveMediaSwitch.isChecked = sensitiveMedia
        binding.visibility.setText(visibility.stringId)
        binding.visibility.setIconResource(visibility.drawableId)
    }

    private fun switchToPreviewMode() {
        editMode = false
        if (spoilerEnabled && spoilerText.isNotBlank()) {
            binding.contentWarning.isEnabled = false
            binding.contentWarningContainer.isVisible = true
            binding.spoilerSwitch.isVisible = false
        } else binding.contentWarningContainer.isVisible = false

        if (content.isNotBlank()) {
            binding.content.isEnabled = false
            binding.contentContainer.isVisible = true
        } else binding.contentContainer.isVisible = false

        if (contentDescription.isNotBlank() && contentDescriptionEnabled) {
            binding.contentDescription.isEnabled = false
            binding.contentDescriptionContainer.isVisible = true
        } else binding.contentDescriptionContainer.isVisible = false

        binding.contentDescriptionSwitch.isVisible = false

        if (sensitiveMedia) {
            binding.sensitiveMediaContainer.isVisible = true
            binding.sensitiveMediaSwitch.isVisible = false
        } else binding.sensitiveMediaContainer.isVisible = false

        binding.visibility.isEnabled = false

        binding.previewActions.isVisible = true
    }

    private fun switchToEditMode() {
        editMode = true

        if (!editModeInitialized) {
            binding.contentWarning.doOnTextChanged { text, _, _, _ -> modifiedSpoilerText = text?.toString() ?: "" }
            binding.spoilerSwitch.setOnCheckedChangeListener { _, isChecked -> modifiedSpoilerEnabled = isChecked }
            binding.content.doOnTextChanged { text, _, _, _ -> modifiedContent = text?.toString() ?: "" }
            binding.contentDescription.doOnTextChanged { text, _, _, _ -> modifiedContentDescription = text?.toString() }
            binding.contentDescriptionSwitch.setOnCheckedChangeListener { _, isChecked -> modifiedContentDescriptionEnabled = isChecked }
            binding.sensitiveMediaSwitch.setOnCheckedChangeListener { _, isChecked -> modifiedSensitiveMedia = isChecked }
            binding.visibility.setOnClickListener { showVisibilityDialog() }
            editModeInitialized = true
        }

        binding.contentWarning.isEnabled = true
        binding.contentWarningContainer.isVisible = true
        binding.spoilerSwitch.isVisible = true
        binding.content.isEnabled = true
        binding.contentContainer.isVisible = true
        binding.contentDescription.isEnabled = true
        binding.contentDescriptionSwitch.isVisible = true
        binding.contentDescriptionContainer.isVisible = true
        binding.sensitiveMediaSwitch.isVisible = true
        binding.sensitiveMediaContainer.isVisible = true
        binding.visibility.isEnabled = true


        binding.previewActions.isVisible = false
    }

    private fun setModifiedVisibility(value: Visibility) {
        modifiedVisibility = value
        binding.visibility.setText(value.stringId)
        binding.visibility.setIconResource(value.drawableId)
    }

    private fun showVisibilityDialog() {
        if (visibilityDialog == null) {
            val dialogBinding = DialogVisibilityBinding.inflate(layoutInflater)
            visibilityDialog = MaterialAlertDialogBuilder(requireContext())
                .setView(dialogBinding.root)
                .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
                .create()
            dialogBinding.visibilityPublic.setOnClickListener {
                setModifiedVisibility(Visibility.PUBLIC)
                visibilityDialog?.dismiss()
            }
            dialogBinding.visibilityUnlisted.setOnClickListener {
                setModifiedVisibility(Visibility.UNLISTED)
                visibilityDialog?.dismiss()
            }
            dialogBinding.visibilityFollowers.setOnClickListener {
                setModifiedVisibility(Visibility.PRIVATE)
                visibilityDialog?.dismiss()
            }
            dialogBinding.visibilityDirect.setOnClickListener {
                setModifiedVisibility(Visibility.DIRECT)
                visibilityDialog?.dismiss()
            }
        }
        visibilityDialog?.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
