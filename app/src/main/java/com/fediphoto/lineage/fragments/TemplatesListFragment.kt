package com.fediphoto.lineage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.fediphoto.lineage.R
import com.fediphoto.lineage.adapters.RecyclerViewItemSpacing
import com.fediphoto.lineage.adapters.TemplatesListAdapter
import com.fediphoto.lineage.databinding.FragmentListBinding
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.fplData
import com.fediphoto.lineage.fplPrefs
import com.fediphoto.lineage.fplTemplateDAO
import com.fediphoto.lineage.viewmodels.TemplatesListViewModel
import com.fediphoto.lineage.viewmodels.TemplatesListViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TemplatesListFragment : Fragment() {
    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!

    private val templatesListViewModel: TemplatesListViewModel by viewModels { TemplatesListViewModelFactory(fplData.getTemplateDAO()) }
    private val adapter: TemplatesListAdapter get() = binding.list.adapter as TemplatesListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.setHasFixedSize(true)
        binding.list.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.list.addItemDecoration(RecyclerViewItemSpacing(binding.list.context, DividerItemDecoration.VERTICAL))
        binding.button.text = getString(R.string.add_status)
        binding.button.setOnClickListener { findNavController().navigate(R.id.templates_to_template) }

        val adapter = TemplatesListAdapter(
            onTemplateEditPressed = this::onTemplateEditPressed,
            onTemplateCopyPressed = this::onTemplateCopyPressed,
            onTemplateActivated = this::onTemplateActivated,
            onTemplateDeleteConfirmed = this::onTemplateDeleteConfirmed
        )
        adapter.activeTemplateId = fplPrefs.activeTemplateId
        binding.list.adapter = adapter
        templatesListViewModel.templates.observe(viewLifecycleOwner) { templatesList ->
            templatesList.let { adapter.submitList(it) }
        }
    }

    private fun onTemplateEditPressed(templateId: Int) =
        findNavController().navigate(TemplatesListFragmentDirections.templatesToTemplate(templateId))

    private fun onTemplateCopyPressed(template: Template) {
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                fplData.getTemplateDAO().insert(
                    template.copy(
                        id = 0,
                        name = getString(R.string.template_name_copy, template.name)
                    )
                )
            }
            withContext(Dispatchers.Main) { binding.list.smoothScrollToPosition(adapter.currentList.lastIndex) }
        }
    }

    private fun onTemplateActivated(templateId: Int) {
        if (fplPrefs.activeTemplateId == templateId) return

        val oldActiveIndex = adapter.currentList.indexOfFirst { it.id == fplPrefs.activeTemplateId }
        val newActiveIndex = adapter.currentList.indexOfFirst { it.id == templateId }

        fplPrefs.activeTemplateId = templateId
        adapter.activeTemplateId = fplPrefs.activeTemplateId

        adapter.notifyItemChanged(oldActiveIndex)
        adapter.notifyItemChanged(newActiveIndex)
    }

    private fun onTemplateDeleteConfirmed(template: Template) {
        lifecycleScope.launch(Dispatchers.IO) {
            if (adapter.currentList.size > 1) {
                val oldIndex = adapter.currentList.indexOfFirst { it.id == template.id }
                val newIndex =
                    if (oldIndex < adapter.currentList.lastIndex)
                        oldIndex + 1
                    else
                        oldIndex - 1
                fplPrefs.activeTemplateId = adapter.currentList[newIndex].id
                adapter.activeTemplateId = fplPrefs.activeTemplateId
                adapter.notifyItemChanged(newIndex)
            } else fplPrefs.activeTemplateId = 0

            fplTemplateDAO().delete(template)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
