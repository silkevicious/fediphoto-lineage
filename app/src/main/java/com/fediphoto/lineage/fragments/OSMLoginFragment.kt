package com.fediphoto.lineage.fragments

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.CookieManager
import android.webkit.WebStorage
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.fediphoto.lineage.OSM_CLIENT_ID
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.REDIRECT_URI
import com.fediphoto.lineage.databinding.FragmentOsmLoginBinding
import com.fediphoto.lineage.datatypes.OSMAccount
import com.fediphoto.lineage.osmGetAccessToken
import com.fediphoto.lineage.osmGetAccountDetails
import com.fediphoto.lineage.showErrorDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class OSMLoginFragment : Fragment() {
    private var _binding: FragmentOsmLoginBinding? = null
    private val binding: FragmentOsmLoginBinding get() = _binding!!

    private lateinit var webView: WebView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentOsmLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.webViewStub.setOnInflateListener { _, inflated: View -> webView = inflated as WebView }
        binding.webViewStub.inflate()
        setDisplayLoadingAnimation(true)
        getAuthorizationCode()
    }


    private fun getAuthorizationCode() {
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                setDisplayLoadingAnimation(true)
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView, url: String) {
                setDisplayLoadingAnimation(!url.startsWith("http"))
                if (url.startsWith(REDIRECT_URI)) {
                    webView.isVisible = false
                    setDisplayLoadingAnimation(true)
                    lifecycleScope.launch(Dispatchers.IO) {
                        kotlin.runCatching { Uri.parse(url).getQueryParameter("code") ?: throw Exception("Authentication parameter not found") }
                            .onSuccess { getAccessToken(it) }
                            .onFailure {
                                withContext(Dispatchers.Main) {
                                    showErrorDialog(requireContext(), it.localizedMessage)
                                    setDisplayLoadingAnimation(false)
                                    it.printStackTrace()
                                }
                            }
                    }
                }
                super.onPageFinished(view, url)
            }
        }
        webView.loadUrl(
            "https://api.openstreetmap.org/oauth2/authorize" +
                    "?client_id=$OSM_CLIENT_ID" +
                    "&redirect_uri=$REDIRECT_URI" +
                    "&response_type=code" +
                    "&scope=read_prefs write_notes"
        )
    }

    private suspend fun getAccessToken(authCode: String) {
        kotlin.runCatching { osmGetAccessToken(authCode) }
            .onSuccess { getAccountDetails(it) }
            .onFailure {
                withContext(Dispatchers.Main) {
                    showErrorDialog(requireContext(), it.localizedMessage)
                    setDisplayLoadingAnimation(false)
                    it.printStackTrace()
                }
            }
    }

    private suspend fun getAccountDetails(accessToken: String) {
        kotlin.runCatching { osmGetAccountDetails(accessToken) }
            .onSuccess { saveAccount(it) }
            .onFailure {
                withContext(Dispatchers.Main) {
                    showErrorDialog(requireContext(), it.localizedMessage)
                    setDisplayLoadingAnimation(false)
                    it.printStackTrace()
                }
            }
    }

    private suspend fun saveAccount(osmAccount: OSMAccount) {
        kotlin.runCatching {
            Prefs(requireActivity().applicationContext).osmAccount = osmAccount
            withContext(Dispatchers.Main) { clearData() }
        }.onSuccess {
            withContext(Dispatchers.Main) { findNavController().popBackStack() }
        }.onFailure {
            withContext(Dispatchers.Main) {
                showErrorDialog(requireContext(), it.localizedMessage)
                setDisplayLoadingAnimation(false)
                it.printStackTrace()
            }
        }
    }

    private fun clearData() {
        WebStorage.getInstance().deleteAllData()
        CookieManager.getInstance().apply {
            removeAllCookies(null)
            flush()
        }
        webView.apply {
            clearCache(true)
            clearHistory()
            clearFormData()
            clearSslPreferences()
        }
    }

    private fun setDisplayLoadingAnimation(display: Boolean) {
        binding.progress.isVisible = display
        webView.isVisible = !display
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
