package com.fediphoto.lineage

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.fediphoto.lineage.databinding.PermissionInfoViewBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder

fun showPermissionDialog(
    context: Context, @DrawableRes icon: Int, @StringRes name: Int, @StringRes description: Int, onRequestButtonPressed: () -> Unit
) {
    val permissionInfoViewBinding = PermissionInfoViewBinding.inflate(LayoutInflater.from(context))
    permissionInfoViewBinding.icon.setImageResource(icon)
    permissionInfoViewBinding.name.setText(name)
    permissionInfoViewBinding.description.setText(description)
    permissionInfoViewBinding.request.isVisible = false
    permissionInfoViewBinding.granted.isVisible = false
    MaterialAlertDialogBuilder(context)
        .setView(permissionInfoViewBinding.root)
        .setPositiveButton(R.string.permission_request) { dialog, _ ->
            onRequestButtonPressed()
            dialog.dismiss()
        }
        .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
        .show()
}

fun getRemoveConfirmationDialog(context: Context, contentView: View, removeClicked: () -> Unit): AlertDialog {
    val materialAlertDialog = MaterialAlertDialogBuilder(context)
        .setView(contentView)
        .setPositiveButton(R.string.remove) { dialog, _ ->
            removeClicked()
            dialog.dismiss()
        }
        .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
        .create()
    materialAlertDialog.setCanceledOnTouchOutside(false)
    return materialAlertDialog
}

fun showErrorDialog(context: Context, error: String?) =
    error?.let { MaterialAlertDialogBuilder(context).setMessage(it).setNeutralButton(R.string.ok) { dialog, _ -> dialog.dismiss() }.show() }
