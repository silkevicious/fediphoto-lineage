package com.fediphoto.lineage.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.widget.TooltipCompat
import androidx.core.view.isVisible
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import coil.load
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.RowAccountBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AccountRow(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {
    private var binding = RowAccountBinding.inflate(LayoutInflater.from(context), this, true)

    fun initialize(
        fplData: FPLData,
        prefs: Prefs,
        lifecycleOwner: LifecycleOwner,
        navigateToLogin: () -> Unit,
        navigateToAccounts: () -> Unit,
    ) {
        lifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            if (fplData.getAccountDAO().isNotEmpty()) {
                val activeAccount = fplData.getAccountDAO().getById(prefs.activeAccountId)
                withContext(Dispatchers.Main) {
                    if (activeAccount != null) {
                        val displayName = activeAccount.displayName.ifBlank { activeAccount.username }
                        val username = "@${activeAccount.username}@${activeAccount.instance}"
                        binding.accountImage.load(activeAccount.avatar) { placeholder(R.drawable.ic_account) }
                        binding.accountDisplayName.text = displayName
                        binding.accountUsername.text = username
                        binding.accountUsername.isVisible = true
                    } else {
                        binding.accountImage.setImageResource(R.drawable.ic_account)
                        binding.accountDisplayName.text = context.getString(R.string.accounts)
                        binding.accountUsername.isVisible = false
                    }
                    binding.accountsButton.contentDescription = context.getString(R.string.accounts)
                    TooltipCompat.setTooltipText(binding.accountsButton, context.getString(R.string.accounts))
                    binding.accountsButton.setIconResource(R.drawable.ic_list)
                    binding.accountsButton.setOnClickListener { navigateToAccounts() }
                }
            } else withContext(Dispatchers.Main) {
                binding.accountDisplayName.text = context.getString(R.string.add_account)
                binding.accountUsername.isVisible = false
                binding.accountsButton.contentDescription = context.getString(R.string.add_account)
                TooltipCompat.setTooltipText(binding.accountsButton, context.getString(R.string.add_account))
                binding.accountsButton.setIconResource(R.drawable.ic_add)
                binding.accountsButton.setOnClickListener { navigateToLogin() }
                binding.accountImage.setImageResource(R.drawable.ic_account)
            }
        }
    }
}
