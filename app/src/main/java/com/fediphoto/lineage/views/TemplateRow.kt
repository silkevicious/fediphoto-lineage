package com.fediphoto.lineage.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.widget.TooltipCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.RowTemplateBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TemplateRow(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {
    private var binding = RowTemplateBinding.inflate(LayoutInflater.from(context), this, true)

    fun initialize(
        fplData: FPLData,
        prefs: Prefs,
        lifecycleOwner: LifecycleOwner,
        navigateToTemplate: () -> Unit,
        navigateToTemplates: () -> Unit,
    ) {
        lifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            if (fplData.getTemplateDAO().isNotEmpty()) {
                val activeTemplate = fplData.getTemplateDAO().getById(prefs.activeTemplateId)
                withContext(Dispatchers.Main) {
                    binding.templateName.text = activeTemplate?.name ?: context.getString(R.string.add_status)
                    binding.templatesButton.contentDescription = context.getString(R.string.status_configs)
                    TooltipCompat.setTooltipText(binding.templatesButton, context.getString(R.string.status_configs))
                    binding.templatesButton.setIconResource(R.drawable.ic_list)
                    binding.templatesButton.setOnClickListener { navigateToTemplates() }
                }
            } else {
                withContext(Dispatchers.Main) {
                    binding.templateName.text = context.getString(R.string.add_status)
                    binding.templatesButton.contentDescription = context.getString(R.string.add_status)
                    TooltipCompat.setTooltipText(binding.templatesButton, context.getString(R.string.add_status))
                    binding.templatesButton.setIconResource(R.drawable.ic_add)
                    binding.templatesButton.setOnClickListener { navigateToTemplate() }
                }
            }
        }
    }
}
