package com.fediphoto.lineage.views

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import com.fediphoto.lineage.LocationService
import com.fediphoto.lineage.R
import com.fediphoto.lineage.databinding.LocationRowBinding
import com.fediphoto.lineage.datatypes.Location
import com.fediphoto.lineage.datatypes.LocationUpdate
import com.fediphoto.lineage.elapsedSeconds
import com.fediphoto.lineage.validOrNull
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlin.time.Duration.Companion.seconds

class LocationRow(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {
    private var binding = LocationRowBinding.inflate(LayoutInflater.from(context), this, true)

    private lateinit var hasLocationPermission: () -> Boolean
    private lateinit var getLocationPermission: () -> Unit
    private lateinit var lifecycleOwner: LifecycleOwner

    private var onLocationServiceBindFailedListener: (() -> Unit)? = null

    private var locationService: LocationService? = null
    var boundToService: Boolean = false
        private set
    private var latestLocationUpdate: MutableLiveData<LocationUpdate?> = MutableLiveData(null)
    var latestLocation: MutableLiveData<Location?> = MutableLiveData(null)
        private set
    private var locationCheckTimer: Job? = null

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as LocationService.LocalBinder
            locationService = binder.getService()
            boundToService = true
            if (locationService?.isRunning == true) {
                locationService?.setOnServiceStop(this@LocationRow::unbindLocationService)
                locationCheckTimer = flow<Nothing> {
                    while (true) {
                        latestLocationUpdate.value = locationService?.getLocationUpdate()
                        latestLocation.value = latestLocationUpdate.value?.validOrNull?.location
                        delay(1.seconds)
                    }
                }.launchIn(lifecycleOwner.lifecycleScope)
                latestLocationUpdate.observe(lifecycleOwner) { updateLocationView(it?.validOrNull?.elapsedSeconds) }
                binding.locationServiceAction.setChipIconResource(R.drawable.ic_stop)
                binding.locationServiceAction.setOnClickListener { stopLocationService() }
            } else {
                onLocationServiceBindFailedListener?.invoke()
                unbindLocationService()
            }
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            unbindLocationService()
            boundToService = false
        }
    }

    fun initialize(locationPermissionCheck: () -> Boolean, locationPermissionRequest: () -> Unit, lifecycleOwner: LifecycleOwner) {
        hasLocationPermission = locationPermissionCheck
        getLocationPermission = locationPermissionRequest
        this.lifecycleOwner = lifecycleOwner
    }

    fun setOnLocationServiceBindFailedListener(onLocationBindingFailedListener: (() -> Unit)?) {
        this.onLocationServiceBindFailedListener = onLocationBindingFailedListener
    }

    fun startAndBindLocationService() {
        if (hasLocationPermission()) {
            ContextCompat.startForegroundService(context, Intent(context, LocationService::class.java))
            bindLocationService()
        } else
            getLocationPermission()
    }

    fun stopLocationService() {
        unbindLocationService()
        ContextCompat.startForegroundService(context, Intent(context, LocationService::class.java).setAction(LocationService.ACTION_STOP))
    }

    fun bindLocationService() {
        context.bindService(Intent(context, LocationService::class.java), connection, Context.BIND_AUTO_CREATE)
    }

    fun unbindLocationService() {
        if (boundToService) {
            locationService?.setOnServiceStop(null)
            locationService = null
            locationCheckTimer?.cancel()
            latestLocationUpdate.removeObservers(lifecycleOwner)
            binding.root.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.card_gps_stopped, context.theme))
            binding.locationIndicatorIcon.setImageResource(R.drawable.ic_gps_off)
            binding.locationIndicatorText.text = null
            binding.locationServiceAction.setChipIconResource(R.drawable.ic_play)
            binding.locationServiceAction.setOnClickListener { startAndBindLocationService() }
            context.unbindService(connection)
            boundToService = false
        }
    }

    private fun updateLocationView(elapsedSeconds: Long?) {
        if (elapsedSeconds != null) {
            binding.root.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.card_gps_available, context.theme))
            binding.locationIndicatorIcon.setImageResource(R.drawable.ic_gps_fixed)
            binding.locationIndicatorText.text = context.getString(R.string.location_state_available).format(elapsedSeconds)
        } else {
            binding.root.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.card_gps_searching, context.theme))
            binding.locationIndicatorIcon.setImageResource(R.drawable.ic_gps_not_fixed)
            binding.locationIndicatorText.text = context.getString(R.string.location_state_searching)
        }
    }
}
