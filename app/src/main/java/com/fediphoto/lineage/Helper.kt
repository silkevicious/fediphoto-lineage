package com.fediphoto.lineage

import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.work.BackoffPolicy
import androidx.work.Constraints
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.WorkRequest
import com.fediphoto.lineage.database.QueueDAO
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.Location
import com.fediphoto.lineage.datatypes.LocationUpdate
import com.fediphoto.lineage.datatypes.QueueItem
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.datatypes.enums.QueueState
import com.fediphoto.lineage.datatypes.enums.Visibility
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.accept
import io.ktor.http.ContentType
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit

const val REDIRECT_URI = "fediphoto://fediphoto"
const val FEDI_REQUESTED_SCOPES = "read write"
const val OSM_CLIENT_ID = "wGVdUTRbb-keFTu8ua2sYGN3fevtpiX7H2LLkb5krvc"

private var systemLanguage: String = ""
lateinit var appLocale: Locale private set

val eiffelTowerLocation = Location(48.85827, 2.29443)
const val CREATED_AT = "created_at"
val olderThanQ = Build.VERSION.SDK_INT < Build.VERSION_CODES.Q

object NetworkLogger : Logger {
    override fun log(message: String) {
        Log.println(Log.INFO, "FPL_DEBUG", message)
    }
}

val httpClient = HttpClient(OkHttp) {
    install(ContentNegotiation) { json(Json { ignoreUnknownKeys = true }) }
    install(Logging) {
        logger = NetworkLogger
        level = LogLevel.ALL
    }
    defaultRequest { accept(ContentType.Application.Json) }
}

fun getLocalizedBaseContext(newBase: Context?): Context? {
    if (newBase != null) {
        if (systemLanguage.isEmpty()) systemLanguage = Locale.getDefault().language
        appLocale = Locale(Prefs(newBase).language.takeIf { it != newBase.getString(R.string.valueLanguageSystem) } ?: systemLanguage)
        ContextWrapper(newBase).resources.configuration.setLocale(appLocale)
    }
    return newBase
}

inline val LocationUpdate.validOrNull: LocationUpdate?
    get() = if (elapsedSeconds < LocationService.LOCATION_UPDATE_INTERVAL * 1.5) this else null

inline val LocationUpdate.elapsedSeconds: Long
    get() = (Date().time - time) / 1000

suspend fun submitWorkerPost(
    context: Context,
    activeAccount: FediAccount,
    activeTemplate: Template,
    queueDAO: QueueDAO,
    photoPath: String,
    location: Location?,
    modifiedSpoilerText: String?,
    modifiedSpoilerEnabled: Boolean?,
    modifiedContent: String?,
    contentDescription: String?,
    modifiedSensitiveMedia: Boolean?,
    modifiedVisibility: Visibility?,
    removeExif: Boolean,
    compressImage: Boolean,
    compressImageQuality: Int,
    osmToken: String?,
    osmNoteText: String?,
    osmOverrideVisibility: Visibility?
) {
    val file = File(photoPath)
    Log.i("submitWorkerPost", "File: ${file.absoluteFile} | Exists: ${file.exists()}")
    val date = file.lastModified().let { lastModified -> if (lastModified != 0L) Date(lastModified) else Date() }

    val queueItem = QueueItem(
        id = 0,
        instance = activeAccount.instance,
        token = activeAccount.token,
        photoPath = photoPath,
        spoilerText = modifiedSpoilerText ?: activeTemplate.spoilerText,
        spoilerEnabled = modifiedSpoilerEnabled ?: activeTemplate.spoilerEnabled,
        text = activeTemplate.text,
        description = contentDescription ?: activeTemplate.defaultContentDescription.takeIf { activeTemplate.defaultContentDescriptionEnabled },
        sensitiveMedia = modifiedSensitiveMedia ?: activeTemplate.sensitiveMedia,
        visibility = modifiedVisibility ?: osmOverrideVisibility.takeIf { osmNoteText != null } ?: activeTemplate.visibility,
        threading = activeTemplate.threading,
        date = activeTemplate.date,
        dateFormat = activeTemplate.dateFormat,
        dateValue = date,
        location = activeTemplate.location,
        locationFormat = activeTemplate.locationFormat,
        locationValue = location,
        modifiedContent = modifiedContent,
        accountId = activeAccount.id,
        templateId = activeTemplate.id,
        removeExif = removeExif,
        exifRemoved = false,
        compressImage = compressImage,
        compressImageQuality = compressImageQuality,
        imageCompressed = false,
        state = QueueState.PENDING,
        modifiedImagePath = null,
        mediaId = null,
        mediaUrl = null,
        statusId = null,
        statusUrl = null,
        osmToken = osmToken?.takeIf { osmNoteText != null },
        osmNoteText = osmNoteText
    )
    queueDAO.insert(queueItem)

    startWorkerPost(context)
}

fun startWorkerPost(context: Context) {
    val constraints = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
    val worker = OneTimeWorkRequest.Builder(WorkerPost::class.java)
        .addTag(WorkerPost.TAG)
        .addTag("$CREATED_AT${System.currentTimeMillis()}")
        .setConstraints(constraints)
        .setBackoffCriteria(BackoffPolicy.LINEAR, WorkRequest.MIN_BACKOFF_MILLIS, TimeUnit.MILLISECONDS)
        .build()
    WorkManager.getInstance(context).enqueueUniqueWork("post", ExistingWorkPolicy.APPEND_OR_REPLACE, worker)
}

fun getThemeMode(context: Context): Int = when (Prefs(context).theme) {
    context.getString(R.string.valueThemeDark) -> AppCompatDelegate.MODE_NIGHT_YES
    context.getString(R.string.valueThemeLight) -> AppCompatDelegate.MODE_NIGHT_NO
    else -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
}

fun createContent(template: Template, date: Date? = null, location: Location? = null): String {
    var content = template.text
    if (template.date && date != null) {
        try {
            val dateString =
                SimpleDateFormat(template.dateFormat, appLocale).format(date).toString()
            if (content.isNotBlank()) content += "\n\n"
            content += dateString
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    if (template.location && location != null) {
        template.locationFormat
            .replace(Template.PATTERN_LOCATION_LATITUDE, location.latitude.toString())
            .replace(Template.PATTERN_LOCATION_LONGITUDE, location.longitude.toString())
            .let { locationString ->
                if (content.isNotBlank()) content += "\n\n"
                content += locationString
            }
    }
    return content
}

fun getFormattedDate(): String = SimpleDateFormat("yyyyMMdd", Locale.US).format(Date())

val Fragment.fplPrefs get() = (requireActivity().application as FPLApplication).prefs
val Fragment.fplData get() = (requireActivity().application as FPLApplication).fplData
fun Fragment.fplAccountDAO() = fplData.getAccountDAO()
fun Fragment.fplTemplateDAO() = fplData.getTemplateDAO()
fun Fragment.fplQueueDAO() = fplData.getQueueDAO()
fun Fragment.fplThreadDAO() = fplData.getThreadDAO()
