package com.fediphoto.lineage

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import androidx.core.database.getStringOrNull
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.QueueItem
import com.fediphoto.lineage.datatypes.StatusThread
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.datatypes.enums.QueueState
import com.fediphoto.lineage.datatypes.enums.Threading
import com.fediphoto.lineage.datatypes.enums.Visibility
import kotlinx.serialization.json.Json
import java.util.Date

class Database(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    companion object {
        const val DB_NAME = "data.sqlite"
        private const val DB_VERSION = 5

        private const val ROW_ID = "rowid"

        private const val TABLE_ACCOUNTS = "accounts"
        private const val ACCOUNTS_COLUMN_AVATAR_URL = "avatar_url"
        private const val ACCOUNTS_COLUMN_USERNAME = "username"
        private const val ACCOUNTS_COLUMN_USER_URL = "url"
        private const val ACCOUNTS_COLUMN_DISPLAY_NAME = "display_name"
        private const val ACCOUNTS_COLUMN_INSTANCE = "instance"
        private const val ACCOUNTS_COLUMN_TOKEN = "token"

        private const val TABLE_TEMPLATES = "templates"
        private const val TEMPLATES_COLUMN_NAME = "name"
        private const val TEMPLATES_COLUMN_SPOILER_TEXT = "spoiler_text"
        private const val TEMPLATES_COLUMN_SPOILER_ENABLED = "spoiler_enabled"
        private const val TEMPLATES_COLUMN_TEXT = "text"
        private const val TEMPLATES_COLUMN_SENSITIVE_MEDIA = "sensitive_media"
        private const val TEMPLATES_COLUMN_VISIBILITY = "visibility"
        private const val TEMPLATES_COLUMN_THREADING = "threading"
        private const val TEMPLATES_COLUMN_DATE = "date"
        private const val TEMPLATES_COLUMN_DATE_FORMAT = "date_format"
        private const val TEMPLATES_COLUMN_LOCATION = "location"
        private const val TEMPLATES_COLUMN_LOCATION_FORMAT = "location_format"

        private const val TABLE_THREADING = "threading"
        private const val THREADING_COLUMN_ACCOUNT_ID = "accountId"
        private const val THREADING_COLUMN_TEMPLATE_ID = "templateId"
        private const val THREADING_COLUMN_LAST_STATUS_ID = "last_status_id"
        private const val THREADING_COLUMN_LAST_STATUS_DATE = "last_status_date"

        private const val TABLE_QUEUE = "queue"
        private const val QUEUE_COLUMN_INSTANCE = "instance"
        private const val QUEUE_COLUMN_TOKEN = "token"
        private const val QUEUE_COLUMN_PHOTO_PATH = "photo_path"
        private const val QUEUE_COLUMN_SPOILER_TEXT = "spoiler_text"
        private const val QUEUE_COLUMN_SPOILER_ENABLED = "spoiler_enabled"
        private const val QUEUE_COLUMN_TEXT = "text"
        private const val QUEUE_COLUMN_SENSITIVE_MEDIA = "sensitive_media"
        private const val QUEUE_COLUMN_VISIBILITY = "visibility"
        private const val QUEUE_COLUMN_THREADING = "threading"
        private const val QUEUE_COLUMN_DATE = "date"
        private const val QUEUE_COLUMN_DATE_FORMAT = "date_format"
        private const val QUEUE_COLUMN_DATE_VALUE = "date_value"
        private const val QUEUE_COLUMN_LOCATION = "location"
        private const val QUEUE_COLUMN_LOCATION_FORMAT = "location_format"
        private const val QUEUE_COLUMN_LOCATION_VALUE = "location_value"
        private const val QUEUE_COLUMN_ACCOUNT_ID = "account_id"
        private const val QUEUE_COLUMN_TEMPLATE_ID = "template_id"
        private const val QUEUE_COLUMN_STATE = "state"
        private const val QUEUE_COLUMN_MEDIA_ID = "media_id"
        private const val QUEUE_COLUMN_MEDIA_URL = "media_url"
        private const val QUEUE_COLUMN_STATUS_ID = "status_id"
        private const val QUEUE_COLUMN_STATUS_URL = "status_url"
        private const val QUEUE_COLUMN_ERROR = "error"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(
            "CREATE TABLE $TABLE_ACCOUNTS(" +
                    "$ACCOUNTS_COLUMN_INSTANCE TEXT," +
                    "$ACCOUNTS_COLUMN_USERNAME TEXT," +
                    "$ACCOUNTS_COLUMN_TOKEN TEXT," +
                    "$ACCOUNTS_COLUMN_USER_URL TEXT," +
                    "$ACCOUNTS_COLUMN_DISPLAY_NAME TEXT," +
                    "$ACCOUNTS_COLUMN_AVATAR_URL TEXT," +
                    "PRIMARY KEY($ACCOUNTS_COLUMN_INSTANCE, $ACCOUNTS_COLUMN_USERNAME)" +
                    ")"
        )
        db?.execSQL(
            "CREATE TABLE $TABLE_TEMPLATES(" +
                    "$TEMPLATES_COLUMN_NAME TEXT," +
                    "$TEMPLATES_COLUMN_SPOILER_TEXT TEXT," +
                    "$TEMPLATES_COLUMN_SPOILER_ENABLED INT," +
                    "$TEMPLATES_COLUMN_TEXT TEXT," +
                    "$TEMPLATES_COLUMN_SENSITIVE_MEDIA INT," +
                    "$TEMPLATES_COLUMN_VISIBILITY INT," +
                    "$TEMPLATES_COLUMN_THREADING INT," +
                    "$TEMPLATES_COLUMN_DATE INT," +
                    "$TEMPLATES_COLUMN_DATE_FORMAT TEXT," +
                    "$TEMPLATES_COLUMN_LOCATION INT," +
                    "$TEMPLATES_COLUMN_LOCATION_FORMAT TEXT" +
                    ")"
        )
        db?.execSQL(
            "CREATE TABLE $TABLE_THREADING(" +
                    "$THREADING_COLUMN_ACCOUNT_ID TEXT," +
                    "$THREADING_COLUMN_TEMPLATE_ID TEXT," +
                    "$THREADING_COLUMN_LAST_STATUS_ID TEXT," +
                    "$THREADING_COLUMN_LAST_STATUS_DATE TEXT," +
                    "PRIMARY KEY($THREADING_COLUMN_ACCOUNT_ID, $THREADING_COLUMN_TEMPLATE_ID)" +
                    ")"
        )
        db?.execSQL(
            "CREATE TABLE $TABLE_QUEUE(" +
                    "$QUEUE_COLUMN_INSTANCE TEXT," +
                    "$QUEUE_COLUMN_TOKEN TEXT," +
                    "$QUEUE_COLUMN_PHOTO_PATH TEXT," +
                    "$QUEUE_COLUMN_SPOILER_TEXT TEXT," +
                    "$QUEUE_COLUMN_SPOILER_ENABLED INT," +
                    "$QUEUE_COLUMN_TEXT TEXT," +
                    "$QUEUE_COLUMN_SENSITIVE_MEDIA INT," +
                    "$QUEUE_COLUMN_VISIBILITY INT," +
                    "$QUEUE_COLUMN_THREADING INT," +
                    "$QUEUE_COLUMN_DATE INT," +
                    "$QUEUE_COLUMN_DATE_FORMAT TEXT," +
                    "$QUEUE_COLUMN_DATE_VALUE TEXT," +
                    "$QUEUE_COLUMN_LOCATION INT," +
                    "$QUEUE_COLUMN_LOCATION_FORMAT TEXT," +
                    "$QUEUE_COLUMN_LOCATION_VALUE TEXT," +
                    "$QUEUE_COLUMN_ACCOUNT_ID INT," +
                    "$QUEUE_COLUMN_TEMPLATE_ID INT," +
                    "$QUEUE_COLUMN_STATE INT," +
                    "$QUEUE_COLUMN_MEDIA_ID TEXT," +
                    "$QUEUE_COLUMN_MEDIA_URL TEXT," +
                    "$QUEUE_COLUMN_STATUS_ID TEXT," +
                    "$QUEUE_COLUMN_STATUS_URL TEXT," +
                    "$QUEUE_COLUMN_ERROR TEXT" +
                    ")"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        if (newVersion > oldVersion) {
            if (oldVersion <= 1) {
                db?.execSQL("ALTER TABLE $TABLE_QUEUE RENAME COLUMN image_path TO $QUEUE_COLUMN_PHOTO_PATH")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE RENAME COLUMN content TO $QUEUE_COLUMN_TEXT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_INSTANCE TEXT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_TOKEN TEXT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_DATE INT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_DATE_FORMAT TEXT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_LOCATION INT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_LOCATION_FORMAT TEXT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_MEDIA_ID TEXT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_MEDIA_URL TEXT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_STATUS_ID TEXT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_STATUS_URL TEXT")
            }
            if (oldVersion <= 2) {
                db?.execSQL("ALTER TABLE $TABLE_TEMPLATES ADD COLUMN $TEMPLATES_COLUMN_SPOILER_TEXT TEXT DEFAULT ''")
                db?.execSQL("ALTER TABLE $TABLE_TEMPLATES ADD COLUMN $TEMPLATES_COLUMN_SPOILER_ENABLED INT")
                db?.execSQL("ALTER TABLE $TABLE_TEMPLATES ADD COLUMN $TEMPLATES_COLUMN_SENSITIVE_MEDIA INT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_SPOILER_TEXT TEXT DEFAULT ''")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_SPOILER_ENABLED INT")
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_SENSITIVE_MEDIA INT")
            }
            if (oldVersion <= 3) {
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_LOCATION_VALUE TEXT")
            }
            if (oldVersion <= 4) {
                db?.execSQL("ALTER TABLE $TABLE_QUEUE ADD COLUMN $QUEUE_COLUMN_DATE_VALUE TEXT")
            }
        }
    }

    fun getAccounts(): MutableList<FediAccount> {
        readableDatabase.apply {
            val accounts = mutableListOf<FediAccount>()
            val cursor = query(
                TABLE_ACCOUNTS,
                arrayOf(
                    ROW_ID,
                    ACCOUNTS_COLUMN_INSTANCE,
                    ACCOUNTS_COLUMN_USERNAME,
                    ACCOUNTS_COLUMN_TOKEN,
                    ACCOUNTS_COLUMN_DISPLAY_NAME,
                    ACCOUNTS_COLUMN_AVATAR_URL
                ),
                null, null, null, null, ROW_ID
            )
            if (cursor.moveToFirst()) {
                do {
                    accounts.add(
                        FediAccount(
                            id = cursor.getInt(cursor.getColumnIndexOrThrow(ROW_ID)),
                            instance = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_INSTANCE)),
                            token = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_TOKEN)),
                            username = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_USERNAME)),
                            displayName = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_DISPLAY_NAME)),
                            avatar = cursor.getString(cursor.getColumnIndexOrThrow(ACCOUNTS_COLUMN_AVATAR_URL))
                        )
                    )
                } while (cursor.moveToNext())
            }
            cursor.close()
            close()
            return accounts
        }
    }

    fun getTemplates(): MutableList<Template> {
        readableDatabase.apply {
            val templates = mutableListOf<Template>()
            val cursor = query(
                TABLE_TEMPLATES,
                arrayOf(
                    ROW_ID,
                    TEMPLATES_COLUMN_NAME,
                    TEMPLATES_COLUMN_SPOILER_TEXT,
                    TEMPLATES_COLUMN_SPOILER_ENABLED,
                    TEMPLATES_COLUMN_TEXT,
                    TEMPLATES_COLUMN_SENSITIVE_MEDIA,
                    TEMPLATES_COLUMN_VISIBILITY,
                    TEMPLATES_COLUMN_THREADING,
                    TEMPLATES_COLUMN_DATE,
                    TEMPLATES_COLUMN_DATE_FORMAT,
                    TEMPLATES_COLUMN_LOCATION,
                    TEMPLATES_COLUMN_LOCATION_FORMAT
                ),
                null, null, null, null, ROW_ID
            )
            if (cursor.moveToFirst()) {
                do {
                    templates.add(
                        Template(
                            id = cursor.getInt(cursor.getColumnIndexOrThrow(ROW_ID)),
                            name = cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_NAME)),
                            spoilerText = cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_SPOILER_TEXT)),
                            spoilerEnabled = cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_SPOILER_ENABLED)) == 1,
                            text = cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_TEXT)),
                            defaultContentDescription = "",
                            defaultContentDescriptionEnabled = true,
                            sensitiveMedia = cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_SENSITIVE_MEDIA)) == 1,
                            visibility = Visibility.values()[cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_VISIBILITY))],
                            threading = Threading.values()[cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_THREADING))],
                            date = cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_DATE)) == 1,
                            dateFormat = cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_DATE_FORMAT)),
                            location = cursor.getInt(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_LOCATION)) == 1,
                            locationFormat = cursor.getString(cursor.getColumnIndexOrThrow(TEMPLATES_COLUMN_LOCATION_FORMAT))
                        )
                    )
                } while (cursor.moveToNext())
            }
            cursor.close()
            close()
            return templates
        }
    }

    fun getThreads(): List<StatusThread> {
        val db = readableDatabase
        val threads = mutableListOf<StatusThread>()
        val cursor = db.query(TABLE_THREADING, null, null, null, null, null, null)
        if (cursor.moveToFirst()) {
            do {
                val accountId = cursor.getString(cursor.getColumnIndexOrThrow(THREADING_COLUMN_ACCOUNT_ID)).toInt()
                val templateId = cursor.getString(cursor.getColumnIndexOrThrow(THREADING_COLUMN_TEMPLATE_ID)).toInt()
                val statusId = cursor.getString(cursor.getColumnIndexOrThrow(THREADING_COLUMN_LAST_STATUS_ID))
                val statusDate = cursor.getString(cursor.getColumnIndexOrThrow(THREADING_COLUMN_LAST_STATUS_DATE))
                threads.add(StatusThread(accountId, templateId, statusId, statusDate))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return threads
    }

    fun getQueueList(): MutableList<QueueItem> {
        readableDatabase.apply {
            val queueList: MutableList<QueueItem> = mutableListOf()
            val cursor = query(
                TABLE_QUEUE,
                arrayOf(
                    ROW_ID,
                    QUEUE_COLUMN_INSTANCE,
                    QUEUE_COLUMN_TOKEN,
                    QUEUE_COLUMN_PHOTO_PATH,
                    QUEUE_COLUMN_SPOILER_TEXT,
                    QUEUE_COLUMN_SPOILER_ENABLED,
                    QUEUE_COLUMN_TEXT,
                    QUEUE_COLUMN_SENSITIVE_MEDIA,
                    QUEUE_COLUMN_VISIBILITY,
                    QUEUE_COLUMN_THREADING,
                    QUEUE_COLUMN_DATE,
                    QUEUE_COLUMN_DATE_FORMAT,
                    QUEUE_COLUMN_DATE_VALUE,
                    QUEUE_COLUMN_LOCATION,
                    QUEUE_COLUMN_LOCATION_FORMAT,
                    QUEUE_COLUMN_LOCATION_VALUE,
                    QUEUE_COLUMN_ACCOUNT_ID,
                    QUEUE_COLUMN_TEMPLATE_ID,
                    QUEUE_COLUMN_STATE,
                    QUEUE_COLUMN_MEDIA_ID,
                    QUEUE_COLUMN_MEDIA_URL,
                    QUEUE_COLUMN_STATUS_ID,
                    QUEUE_COLUMN_STATUS_URL,
                    QUEUE_COLUMN_ERROR
                ),
                null,
                null,
                null, null, ROW_ID
            )
            if (cursor.moveToFirst()) {
                do {
                    queueList.add(
                        QueueItem(
                            id = cursor.getLong(cursor.getColumnIndexOrThrow(ROW_ID)),
                            instance = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_INSTANCE)),
                            token = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_TOKEN)),
                            photoPath = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_PHOTO_PATH)),
                            spoilerText = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_SPOILER_TEXT)),
                            spoilerEnabled = cursor.getInt(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_SPOILER_ENABLED)) == 1,
                            text = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_TEXT)),
                            description = null,
                            sensitiveMedia = cursor.getInt(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_SENSITIVE_MEDIA)) == 1,
                            visibility = Visibility.values()[cursor.getInt(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_VISIBILITY))],
                            threading = Threading.values()[cursor.getInt(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_THREADING))],
                            date = cursor.getInt(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_DATE)) == 1,
                            dateFormat = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_DATE_FORMAT)),
                            dateValue = cursor.getStringOrNull(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_DATE_VALUE))?.toLongOrNull()?.let { Date(it) },
                            location = cursor.getInt(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_LOCATION)) == 1,
                            locationFormat = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_LOCATION_FORMAT)),
                            locationValue = cursor.getStringOrNull(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_LOCATION_VALUE))?.let { Json.decodeFromString(it) },
                            modifiedContent = null,
                            accountId = cursor.getInt(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_ACCOUNT_ID)),
                            templateId = cursor.getInt(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_TEMPLATE_ID)),
                            removeExif = true,
                            exifRemoved = false,
                            compressImage = false,
                            compressImageQuality = 80,
                            imageCompressed = false,
                            state = QueueState.values()[cursor.getInt(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_STATE))],
                            modifiedImagePath = null,
                            mediaId = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_MEDIA_ID)),
                            mediaUrl = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_MEDIA_URL)),
                            statusId = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_STATUS_ID)),
                            statusUrl = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_STATUS_URL)),
                            error = cursor.getString(cursor.getColumnIndexOrThrow(QUEUE_COLUMN_ERROR))
                        )
                    )
                } while (cursor.moveToNext())
            }
            cursor.close()
            close()
            return queueList
        }
    }
}
