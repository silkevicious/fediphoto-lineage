package com.fediphoto.lineage

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.work.WorkManager
import com.fediphoto.lineage.databinding.ActivityMainBinding
import com.fediphoto.lineage.fragments.HomeFragmentDirections
import java.util.Date
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    private var actionButtonsVisible = true
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun attachBaseContext(newBase: Context?) = super.attachBaseContext(getLocalizedBaseContext(newBase))

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, true)
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.fragment_container)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            actionButtonsVisible = (destination.id == R.id.HomeFragment)
            invalidateMenu()
        }

        if (savedInstanceState == null) workerStatus()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (actionButtonsVisible) menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val navController = findNavController(R.id.fragment_container)
        return when (item.itemId) {
            R.id.action_about -> {
                navController.navigate(HomeFragmentDirections.homeToAbout())
                true
            }

            R.id.action_settings -> {
                navController.navigate(HomeFragmentDirections.homeToSettings())
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragment_container)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST && permissions.isNotEmpty() && grantResults.isNotEmpty()) {
            for (i in permissions.indices) {
                when (permissions[i]) {
                    Manifest.permission.CAMERA -> {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                            Toast.makeText(this, R.string.camera_permission_granted, Toast.LENGTH_LONG).show()
                        else
                            Toast.makeText(this, R.string.camera_permission_denied, Toast.LENGTH_LONG).show()
                    }

                    Manifest.permission.WRITE_EXTERNAL_STORAGE -> {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                            Toast.makeText(this, R.string.external_storage_permission_granted, Toast.LENGTH_LONG).show()
                        else
                            Toast.makeText(this, R.string.external_storage_permission_denied, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun workerStatus() {
        val workInfoList = WorkManager.getInstance(this).getWorkInfosByTag(WorkerPost.TAG)
        var quantityNotFinishedDayOrOlder = 0
        try {
            val workInfos = workInfoList[5, TimeUnit.SECONDS]
            var quantityNotFinished = 0
            val isDebuggable = 0 != applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE
            for (workInfo in workInfos) {
                if (workInfo != null && !workInfo.state.isFinished) {
                    var createdMilliseconds: Long = 0
                    val tags = workInfo.tags
                    for (workerTag in tags) {
                        Log.i(TAG, String.format("Worker tag: %s", workerTag))
                        if (workerTag.startsWith(CREATED_AT))
                            createdMilliseconds = workerTag.substring(CREATED_AT.length).ifBlank { "0" }.toLong()
                    }
                    Log.i(
                        TAG,
                        String.format(
                            "Worker created %s info %s.",
                            Date(createdMilliseconds),
                            workInfo.toString()
                        )
                    )
                    quantityNotFinished++
                    if (createdMilliseconds > 0
                        && (System.currentTimeMillis() - createdMilliseconds > 60 * 60 * 24 * 1000 /* Milliseconds in a day */
                                || isDebuggable && System.currentTimeMillis() - createdMilliseconds > 60 * 1000 /* Milliseconds in a minute */)
                    ) {
                        quantityNotFinishedDayOrOlder++
                    }
                }
            }
            Log.i(
                TAG,
                String.format(
                    "%d worker info quantity. %d not finished.",
                    workInfos.size,
                    quantityNotFinished
                )
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (quantityNotFinishedDayOrOlder > 0) {
            promptUserToDeleteWorkers(quantityNotFinishedDayOrOlder)
        }
    }

    private fun promptUserToDeleteWorkers(quantity: Int) {
        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    WorkManager.getInstance(this).cancelAllWork()
                    val message = String.format(appLocale, getString(R.string.workers_cancelled), quantity)
                    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                }

                DialogInterface.BUTTON_NEGATIVE -> {
                }
            }
        }
        val message = String.format(appLocale, getString(R.string.older_workers_prompt), quantity)
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message).setPositiveButton(R.string.yes, dialogClickListener)
            .setNegativeButton(R.string.no, dialogClickListener).show()
    }

    companion object {
        private const val PERMISSION_REQUEST = 1
        private const val TAG = "MainActivity"
    }
}
