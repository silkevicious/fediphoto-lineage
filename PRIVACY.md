## Privacy Policy

* Account information, including user name, instance domain name and OAuth token is stored on the device in a file named settings.json. When you delete this app, the account information is destroyed.

* Photos are stored on the device and posted to the Fediverse. A setting allows for photos after posting to be deleted, moved, or copied to the Android Pictures folder.

* Sharing photo location is optional. The latitude and longitude is extracted from the photo using ExifInterface only if you have choosen to store location in your photos. You can configure a status to use latitude and logitude using two format arguments %.5f.

* Sharing a photo timestamp is optional. You can modify the timestamp format using Java's Simple Date Format syntax.

* All transmissions are over SSL.

### Required Permissions

The app uses the minimum permission required to work, some of them are mandatory. A brief explaination of what we use it for:

* android.permission.CAMERA : well, it's what the app is for so...should we explain it? (mandatory)

* android.permission.INTERNET : we would like to have internet access to post your images. (mandatory)

* android.permission.ACCESS_FINE_LOCATION : this is for posting the photo location. It's not mandatory so you can decide if you concede it or not.

* android.permission.WRITE_EXTERNAL_STORAGE : used to store your photo on the phone, mandatory up to Android 9. Since 10 isn't requested anymore because Android changes.