Moitas novidades! Debes desinstalar versións anteriores antes de usar esta.
- Utilizamos unha base de datos no lugar de ficheiros json (#13)
- Arranxo dos problemas coa almacenaxe en sistemas con idioma diferente a en-us (#14)
- Facer máis claro que hai que engadir unha conta e configuración de estado (#2 e #10)
- Non se amosaba a configuración de Conta-Estado (#8)
- Perfil establecido como activo aínda non tendo a marca (#11)
- Fotos publicadas coa conta errónea (#7)
