App Android per pubblicare velocemente foto nel Fediverso.

PLA ha scritto questa app per riuscire a pubblicare foto nel Fediverso facilmente durante i suoi giri in bici.
Noi stiamo continuando il suo progetto per onorare la sua memoria.

Configura il testo e le etichette per lo stato prima di partire. Poi con 3 tap puoi pubblicare una foto:
* Premi per la fotocamera.
* Inquadra il soggetto e premi per scattare la foto.
* Premi per accettare la foto e...fatto!

La pubblicazione della foto e del testo è eseguita in background.
Se c'è poco segnale sarà caricata quando la connessione lo consentirà.

Caratteristiche:
- Possono essere aggiunti più account
- Possono essere impostate più configurazioni dello stato
- Anteprima prima della pubblicazione o pubblicazione immediata dopo lo scatto
- Servizio di posizione incluso (nel caso la fotocamera non fornisca la posizione)
- Modalità offline (carica messaggi/foto quando internet è di nuovo disponibile)
- Gestisci foto scattate (mantieni o elimina)
- Crea note OSM con le tue foto
- Importa/Esporta impostazioni e account

https://silkevicious.codeberg.page/fediphoto-lineage.html
